import React, {Component} from 'react';
import {connect} from 'react-redux';

import './EventsMap.css';

class EventsMap extends Component {

	constructor(props) {
		super(props);

		this.GMAPI = null;
		this.markers = [];
		this.mapRef = React.createRef();

		this.state = {
			fav: false
		};
	}

	init = () => {
		const {current: mapDiv} = this.mapRef;
		const {
			zoom = 14,
			center
		} = this.props;

		if (this.GMAPI == null) {
			this.GMAPI = {
				map: new window.google.maps.Map(
					mapDiv,
					{
						zoom,
						center
					},
				),
			};
		}
	};

	clearMap = () => {
		for (var i = 0; i < this.markers.length; i++) {
			this.markers[i].setMap(null);
		}
		this.markers = [];
	};

	bindEvents = () => {
		this.clearMap();
		const {events} = this.props;
		if (events && this.GMAPI) {
			const map = this.GMAPI.map;
			const info = new window.google.maps.InfoWindow({});

			events.forEach(({Route, id, name, term, description}) => {
				const position = {
					lng: Route.start.coordinates[0],
					lat: Route.start.coordinates[1],
				};

				const marker = new window.google.maps.Marker({
					position,
					map,
					icon: './pin.png',
				});

				this.markers.push(marker);

				const date = new Date(term);

				const content = `<div> 
						<h3>${name}</h3>
						<p>${date.toLocaleDateString()} v ${date.toLocaleTimeString()}</p>
						<p>${description}</p>
						<a href="/event/${id}" class="btn btn-link">Event detail</a>
					</div>`;

				window.google.maps.event.addListener(marker, 'click', function () {
					info.setContent(content);
					info.open(map, this);
				});
			});
		}
	};

	componentDidMount() {
		window.MapLoader.ready(this.init);
	}

	componentDidUpdate() {
		this.bindEvents();
	}

	render() {
		return (
			<div className="eventsMap">
				<div
					style={{ height: '50vh', width: '100%' }}
					ref={this.mapRef}
				/>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const {startingPosition} = state.auth;
	const {lat = 50.085343, lng = 14.435975} = startingPosition || {};
	return {
		center: {
			lat,
			lng,
		},
	}
};

export default connect(mapStateToProps)(EventsMap);
