import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import 'popper.js';
import 'jquery';
import 'bootstrap';

import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

import axios from './axios';
import history from './services/history';
import apiInterceptor from './services/apiInterceptor';
import AppRoutes from './AppRoutes/AppRoutes';
import { configureStore } from './store/configureStore.js';
const { store } = configureStore();

apiInterceptor(axios, store);

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<Router history={history}>
					<AppRoutes />
				</Router>
			</Provider>
		);
	}
}

export default App;
