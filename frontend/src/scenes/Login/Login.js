import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';

import { Validator, IS_EMPTY, IS_EMAIL } from '../../utils/Validator/Validator';
import FlashMessageBox from '../../components/FlashMessageBox/FlashMessageBox';
import { auth } from '../../services/Auth/authActions';

import './Login.css';

class Login extends Component {
	state = {
		form: {
			email: '',
			password: '',
			submitted: false,
		},
	};

	validator = new Validator([
		{
			field: 'email',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'E-mail nemůže být prázdný.',
				},
				{
					method: IS_EMAIL,
					validWhen: true,
					errorMsg: 'Zadejte prosím platný E-mail.',
				},
			],
		},
		{
			field: 'password',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'Heslo nemůže být prázdné.',
				},
			],
		},
	]);

	onChangeHandler = event => {
		const fieldName = event.target.name;
		const value = event.target.value;
		const form = {
			...this.state.form,
			[fieldName]: value,
		};
		this.setState({ form });
	};

	onSubmitHandler = event => {
		event.preventDefault();

		const form = {
			...this.state.form,
			submitted: true,
		};
		const { email, password } = this.state.form;
		this.setState({ form });
		if (this.validator.validate(form)) {
			this.props.onSubmit({ email, password });
		}
	};

	render() {
		const { isLoggedIn, flashMessages } = this.props;
		const { email, password } = this.state.form;

		return isLoggedIn ? (
			<Redirect to="/" />
		) : (
			<div className="login">
				<div className="login-container">
					<FlashMessageBox flashMessages={flashMessages} />
					<form onSubmit={this.onSubmitHandler}>
						<div className="form-group">
							<label>E-Mail</label>
							<input
								type="text"
								className={this.validator.getClassNames(
									'email',
								)}
								name="email"
								value={email}
								onChange={this.onChangeHandler}
							/>
							<span className="help-block">
								{this.validator.getErrorMessage('email')}
							</span>
						</div>
						<div className="form-group">
							<label>Password</label>
							<input
								type="password"
								className={this.validator.getClassNames(
									'password',
								)}
								name="password"
								onChange={this.onChangeHandler}
								value={password}
							/>
							<span className="help-block">
								{this.validator.getErrorMessage('password')}
							</span>
						</div>
						<button
							type="submit"
							className="btn btn-info btn-block"
						>
							Log in
						</button>
						<div className="signup-link text-center">
							<Link to="/signup">Create an account and start running!</Link>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { token } = state.auth;
	const { flashMessages } = state.share;

	return {
		isLoggedIn: token,
		flashMessages,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onSubmit: payload => dispatch(auth(payload)),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Login);
