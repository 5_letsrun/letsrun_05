'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface
			.createTable('events', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				name: {
					allowNull: false,
					type: Sequelize.STRING(50),
				},
				description: {
					allowNull: false,
					type: Sequelize.TEXT,
				},
				term: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				route_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				user_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				created_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updated_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			})
			.then(() => {
				queryInterface.addConstraint('events', ['route_id'], {
					type: 'foreign key',
					references: {
						table: 'routes',
						field: 'id',
					},
				});

				queryInterface.addConstraint('events', ['user_id'], {
					type: 'foreign key',
					references: {
						table: 'users',
						field: 'id',
					},
				});

				queryInterface.addIndex('events', ['term']);
			});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('events');
	},
};
