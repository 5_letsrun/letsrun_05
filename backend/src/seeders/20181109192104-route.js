'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		const routesPromise = queryInterface.bulkInsert(
			'routes',
			[
				{
					id: 1,
					start: Sequelize.literal(
						"ST_GeomFromText('POINT(39.807222 76.8456)')",
					),
					trace: JSON.stringify({
						type: 'Feature',
						geometry: {
							type: 'Point',
							coordinates: [125.6, 10.1],
						},
						properties: {
							name: 'Lorem Islands',
						},
					}),
					distance: 10,
					user_id: 1,
					created_at: new Date(),
					updated_at: new Date(),
				},
				{
					id: 2,
					start: Sequelize.literal(
						"ST_GeomFromText('POINT(54.807222 19.8456)')",
					),
					trace: JSON.stringify({
						type: 'Feature',
						geometry: {
							type: 'Point',
							coordinates: [125.6, 10.1],
						},
						properties: {
							name: 'Ipsum Islands',
						},
					}),
					distance: 30,
					user_id: 2,
					created_at: new Date(),
					updated_at: new Date(),
				},
			],
			{},
		);

		return Promise.all([routesPromise]);
	},

	down: (queryInterface, Sequelize) => {
		return Promise.all([queryInterface.bulkDelete('routes', null, {})]);
	},
};
