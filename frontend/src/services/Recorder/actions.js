export const RECORDER_SET_TIME = 'RECORDER_SET_TIME';

export const setTime = time => ({
	type: RECORDER_SET_TIME,
	payload: { time },
});
