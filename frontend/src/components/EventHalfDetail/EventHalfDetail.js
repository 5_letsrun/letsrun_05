import React, { Component } from 'react';
import Map from '../Map/Map';

export default class EventHalfDetail extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		return (
			<section>
				<Map
					id={'EventDetailMap' + this.props.id}
					zoom={15}
					showLabels={false}
					routeId={this.props.routeId}
				/>
			</section>
		);
	}
}
