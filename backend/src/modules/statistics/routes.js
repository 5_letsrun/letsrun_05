import expressAsyncAwait from 'express-async-await';
import { Router } from 'express';

import { getStatisticsData } from './statisticsController';

import { setGoal } from './goalController';
import { getGoal } from './goalController';
import { getCurrentDistance } from './goalController';

const router = expressAsyncAwait(Router());

router.get('/', getStatisticsData);
router.post('/goal', setGoal);
router.get('/goal', getGoal);
router.get('/currentDistance', getCurrentDistance);

export default router;
