import dotenv from 'dotenv-override';
import path from 'path';
import {rootDir} from '../../../frontend/src/paths';

dotenv.config({
	path: path.resolve(rootDir, '.env'),
	override: true,
});

const env = process.env.NODE_ENV || 'development';

const Sequelize = require('sequelize');
const config = require(__dirname + '/../config/config.json')[env];
const DataTypes = require('sequelize/lib/data-types');

let db = {};
let sequelize = {};

if (config.use_env_variable) {
	sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
	sequelize = new Sequelize(
		config['database'],
		config['username'],
		config['password'],
		config,
	);
}

// Add all modules to this array
const modelModules = [
	require('./user'),
	require('./route'),
	require('./event'),
	require('./participant'),
	require('./favourite-route'),
	require('./favourite-user'),
	require('./password-reset'),
	require('./run'),
	require('./comment'),
	require('./goal'),
];

modelModules.forEach(modelModule => {
	const model = modelModule(sequelize, DataTypes);
	db[model.name] = model;
});

Object.keys(db).forEach(modelName => {
	if (db[modelName].associate) {
		db[modelName].associate(db);
	}
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
