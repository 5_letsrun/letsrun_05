import React from 'react';

import FlashMessage from './FlashMessage/FlashMessage';

const FlashMessageBox = ({ flashMessages }) => {
	return Object.keys(flashMessages).map(key => {
		const { type, message } = flashMessages[key];
		return (
			<FlashMessage key={key} type={type}>
				{message}
			</FlashMessage>
		);
	});
};

export default FlashMessageBox;
