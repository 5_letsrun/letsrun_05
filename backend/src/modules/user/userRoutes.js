import expressAsyncAwait from 'express-async-await';
import { Router } from 'express';

import {
	getAllUsers,
	editUser,
	isUserRegisteredForEvent,
	getFavouriteUsers,
	addToFavouriteUsers,
	removeFromFavouriteUsers,
	getUserName,
} from './userController';

const router = expressAsyncAwait(Router());
router.get('/getAll', getAllUsers);
router.post('/edit', editUser);
router.get('/isUserRegisteredForEvent', isUserRegisteredForEvent);
router.get('/:id/favourite', getFavouriteUsers);
router.post('/:id/favourite/:friendId', addToFavouriteUsers);
router.delete('/:id/favourite/:friendId', removeFromFavouriteUsers);
router.get('/getUserName', getUserName);

export default router;
