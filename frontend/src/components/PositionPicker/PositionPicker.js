import React, { Component } from 'react';

import './PositionPicker.css';

import {connect} from 'react-redux';

class PositionPicker extends Component {
	constructor(props) {
		super(props);

		this.GMAPI = null;
		this.marker = null;
		this.mapRef = React.createRef();
	}

	componentDidMount() {
		window.MapLoader.ready(this.init);
	}

	pick = e => {
		let point = { lat: e.latLng.lat(), lng: e.latLng.lng() };
		if(this.marker == null) {
			this.marker = new window.google.maps.Marker({
				position: point,
				label: {
					color: 'white',
					text: 'X',
				},
				map: this.GMAPI.map,
				draggable: true,
			});
			this.marker.addListener('dragend', (e) => {
				this.output({ lat: e.latLng.lat(), lng: e.latLng.lng() });
			});
		} else {
			this.marker.setPosition(point);
		}
		this.output(point);
	};

	output = point => {
		const {onClick} =  this.props;
		onClick(point);
	};

	init = () => {
		const {current: mapDiv} = this.mapRef;
		const {
			zoom = 14,
			center
		} = this.props;

		if (this.GMAPI === null) {
			this.GMAPI = {
				map: new window.google.maps.Map(
					mapDiv,
					{
						zoom,
						center
					},
				)
			};
		}

		this.GMAPI.map.addListener('click', this.pick);
	};

	render() {
		return (
				<div
					ref={this.mapRef}
					className='positionPicker'
				/>
		);
	}
}

const toProps = state => {
	const { startingPosition } = state.auth;
	const {lat = 50.085343, lng = 14.435975 } = startingPosition || {};
	return { center: { lat, lng } }
}

export default connect(toProps)(PositionPicker);
