import React from 'react';

export default props => {
	const { name, email, datetime, content } = props;

	return (
		<li className="list-group-item">
			<p className="font-weight-bold">
				{name} ({email})
			</p>
			<p className="font-italic">{datetime}</p>
			{content}
		</li>
	);
};
