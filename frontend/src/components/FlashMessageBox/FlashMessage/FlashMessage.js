import React from 'react';
import className from 'classnames';
import './FlashMessage.css';

export const FLASH_SUCCESS = 'success';
export const FLASH_DANGER = 'danger';
export const FLASH_INFO = 'info';

const FlashMessage = props => {
	const classNames = className('alert', 'alert-' + props.type);
	return (
		props.children &&
		<div className={classNames} role="alert">
			{props.children}
		</div>
	);
};

export default FlashMessage;
