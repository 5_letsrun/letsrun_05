import React, { Component } from 'react';
import Axios from '../../axios';
import { connect } from 'react-redux';
import { RunItem } from '../Items/RunItem';
import { EventItem } from '../Items/EventItem';
import { ListHeader } from '../Items/ListHeader.js';
// import { getYear } from '../../services/Statistics/reducer.js';
import { setCurrentDistance } from '../../services/Statistics/actions.js';
import './RunItemList.css';

export class RunItemList extends Component {
	constructor(props) {
		super(props);

		const date = new Date();
		const currentMonth = date.getMonth() + 1;
		const currentYear = date.getFullYear();

		this.state = {
			runs: [],
			events: [],
			year: currentYear,
			month: currentMonth,
			user: this.props.user,
			currentDistance: null,
		};
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			prevProps.year !== this.props.year ||
			prevProps.month !== this.props.month
		) {
			let runs = [];
			let events = [];

			Axios.get(`/statistics`, {
				params: {
					month: this.props.month,
					year: this.props.year,
					user: this.props.statisticsUser,
				},
			}).then(response => {
				runs = response.data.runs;
				events = response.data.events;
				this.setState({ runs: runs, events: events });
				this.setState({
					year: this.props.year,
					month: this.props.month,
				});
			});
		}
	}

	componentDidMount() {
		let runs = [];
		let events = [];
		// let currentDistance;
		Axios.get(`/statistics`, {
			params: {
				month: this.state.month,
				year: this.state.year,
				user: this.props.statisticsUser,
			},
		}).then(response => {
			runs = response.data.runs;
			events = response.data.events;
			this.setState({ runs: runs, events: events });
		});
	}

	render() {
		const runs = this.state.runs;
		const events = this.state.events;
		let runsPart;
		let eventsPart;

		if (runs != null) {
			runsPart = (
				<div id="runs">
					{this.state.runs ? (
						runs.map(run => <RunItem run={run} key={run.id} />)
					) : (
						<div>Loading ...</div>
					)}
				</div>
			);
		}

		if (events != null) {
			eventsPart = (
				<div id="events">
					{this.state.events ? (
						events.map(event => (
							<EventItem event={event} key={event.id} />
						))
					) : (
						<div>Loading ...</div>
					)}
				</div>
			);
		}

		return (
			<div className="statisticslist">
				<ListHeader />
				{eventsPart}
				{runsPart}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		year: state.statistics.year,
		month: state.statistics.month,
		user: state.auth.id,
	};
};

const mapDispatchToProps = {
	setCurrentDistance,
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(RunItemList);
