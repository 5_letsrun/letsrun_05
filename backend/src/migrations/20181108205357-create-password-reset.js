'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface
			.createTable('password_resets', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				email: {
					allowNull: false,
					type: Sequelize.STRING(60),
				},
				token: {
					allowNull: false,
					type: Sequelize.STRING(60),
				},
				created_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updated_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			})
			.then(() => {
				queryInterface.addIndex('password_resets', ['email']);
			});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('password_resets');
	},
};
