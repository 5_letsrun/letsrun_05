'use strict';
module.exports = (sequelize, DataTypes) => {
	const Participant = sequelize.define(
		'Participant',
		{
			userId: {
				type: DataTypes.INTEGER.UNSIGNED,
				allowNull: false,
				field: 'user_id',
			},
			eventId: {
				type: DataTypes.INTEGER.UNSIGNED,
				allowNull: false,
				field: 'event_id',
			},
		},
		{
			underscored: true,
			tableName: 'participants',
		},
	);
	Participant.associate = function(models) {
		Participant.belongsTo(models.Event);

		Participant.belongsTo(models.User);
	};
	return Participant;
};
