'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		const participantsPromise = queryInterface.bulkInsert(
			'participants',
			[
				{
					event_id: 1,
					user_id: 1,
					created_at: new Date(),
					updated_at: new Date(),
				},
				{
					event_id: 1,
					user_id: 2,
					created_at: new Date(),
					updated_at: new Date(),
				},
				{
					event_id: 2,
					user_id: 2,
					created_at: new Date(),
					updated_at: new Date(),
				},
			],
			{},
		);

		return Promise.all([participantsPromise]);
	},

	down: (queryInterface, Sequelize) => {
		return Promise.all([
			queryInterface.bulkDelete('participants', null, {}),
		]);
	},
};
