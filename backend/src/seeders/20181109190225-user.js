'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		const usersPromise = queryInterface.bulkInsert(
			'users',
			[
				{
					id: 1,
					email: 'krid03@vse.cz',
					first_name: 'David',
					last_name: 'Kristín',
					full_name: 'David Kristín',
					password:
						'$2a$10$KzVDeV20lnzQsSxMarnKbuMuZWoIMVHB8IKLMhHdz7DRSJHYb8y0i', // secret
					created_at: new Date(),
					updated_at: new Date(),
				},
				{
					id: 2,
					email: 'john@doe.com',
					first_name: 'John',
					last_name: 'Doe',
					full_name: 'David Kristín',
					password:
						'$2a$10$KzVDeV20lnzQsSxMarnKbuMuZWoIMVHB8IKLMhHdz7DRSJHYb8y0i', // secret
					created_at: new Date(),
					updated_at: new Date(),
				},
			],
			{},
		);

		return Promise.all([usersPromise]);
	},

	down: (queryInterface, Sequelize) => {
		return Promise.all([queryInterface.bulkDelete('users', null, {})]);
	},
};
