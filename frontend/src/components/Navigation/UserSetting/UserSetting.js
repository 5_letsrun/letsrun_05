import React from 'react';
import {Link} from 'react-router-dom';

import FontIcon from '../../FontIcon/FontIcon';
import './UserSetting.css';

export default props => (
	<div className="dropdown">
		<button
			className="btn btn-secondary dropdown-toggle"
			type="button"
			id="dropdownMenuButton"
			data-toggle="dropdown"
			aria-haspopup="true"
			aria-expanded="false"
		>
			<FontIcon icon="user" />
		</button>
		<div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<Link to="/userSetting" className="dropdown-item">Profile</Link>
			<Link to="/logout" className="dropdown-item">Log Out</Link>
		</div>
	</div>
);
