import axios from '../../axios';
import { createFlashMessage } from '../Share/shareActions';
import {
	FLASH_DANGER,
	FLASH_SUCCESS,
} from '../../components/FlashMessageBox/FlashMessage/FlashMessage';
import history from "../history";

export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const LOGOUT = 'LOGOUT';
export const UPDATE_USER = 'UPDATE_USER';

const authStart = () => {
	return {
		type: AUTH_START,
	};
};

const authSuccess = payload => {
	return {
		type: AUTH_SUCCESS,
		payload,
	};
};

const updateUser = payload => {
	return {
		type: UPDATE_USER,
		payload
	};
};

const signUpSuccess = payload => {
	return {
		type: SIGNUP_SUCCESS,
		payload,
	};
};

const logout = () => {
	return {
		type: LOGOUT,
	};
};

export const processLogout = (payload = {}) => {
	const {error, showMessage = true, redirect = false} = payload;
	return dispatch => {
		localStorage.clear();

		if (showMessage) {
			if (error) {
				dispatch(createFlashMessage({
					type: FLASH_DANGER,
					message: error,
				}));
			} else {
				dispatch(createFlashMessage({
					type: FLASH_SUCCESS,
					message: 'You have been successfully log out.',
				}));
			}
		}

		dispatch(logout());

		if (redirect) {
			history.push('/login');
		}
	};
};


export const auth = payload => {
	return dispatch => {
		dispatch(authStart());

		axios
			.post('/auth/login', payload)
			.then(response => {
				const {
					id,
					email,
					firstName,
					lastName,
					token,
					startingPosition,
				} = response.data.user;

				localStorage.setItem('id', id);
				localStorage.setItem('email', email);
				localStorage.setItem('firstName', firstName);
				localStorage.setItem('lastName', lastName);
				localStorage.setItem('startingPosition', JSON.stringify(startingPosition));
				localStorage.setItem('token', token);

				dispatch(
					authSuccess({ id, email, firstName, lastName, token, startingPosition }),
				);
			})
			.catch(errorResponse => {
				const { error } = errorResponse.response.data;
				dispatch(
					createFlashMessage({
						type: FLASH_DANGER,
						message: error,
					}),
				);
			});
	};
};

export const autoCheckState = () => {
	return dispatch => {
		const token = localStorage.getItem('token');
		const id = localStorage.getItem('id');
		const email = localStorage.getItem('email');
		const firstName = localStorage.getItem('firstName');
		const lastName = localStorage.getItem('lastName');
		const startingPosition = JSON.parse(localStorage.getItem('startingPosition'));

		if (token && id && email && firstName && lastName) {
			dispatch(authSuccess({ id, email, firstName, lastName, token, startingPosition }));
		} else {
			dispatch(processLogout({ showMessage: false}));
		}
	};
};

export const signUp = payload => {
	return dispatch => {
		axios
			.post('/auth/signUp', payload)
			.then(() => {
				dispatch(
					signUpSuccess({
						signUpSuccess: true,
					}),
				);
				dispatch(
					createFlashMessage({
						type: FLASH_SUCCESS,
						message:
							'Registration was successful. Login with your new account.',
					}),
				);
			})
			.catch(errorResponse => {
				const { error } = errorResponse.response.data;
				dispatch(
					signUpSuccess({
						signUpSuccess: false,
					}),
				);
				dispatch(
					createFlashMessage({
						type: FLASH_DANGER,
						message: error,
					}),
				);
			});
	};
};

export const processUpdateUser = payload => {
	return dispatch => {
		axios
			.post('/user/edit', payload)
			.then(res => {
				const {
					id,
					email,
					firstName,
					lastName,
					startingPosition,
				} = res.data.user;

				localStorage.setItem('id', id);
				localStorage.setItem('email', email);
				localStorage.setItem('firstName', firstName);
				localStorage.setItem('lastName', lastName);
				localStorage.setItem('startingPosition', JSON.stringify(startingPosition));

				dispatch(updateUser({
					id,
					email,
					firstName,
					lastName,
					startingPosition,
				}));

				dispatch(
					createFlashMessage({
						type: FLASH_SUCCESS,
						message:
							'Your profile has been successfully updated.',
					}),
				);
			})
			.catch(errorResponse => {
				const { error } = errorResponse.response.data;
				dispatch(
					createFlashMessage({
						type: FLASH_DANGER,
						message: error,
					}),
				);
			});
	};
};
