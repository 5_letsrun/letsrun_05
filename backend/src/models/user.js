'use strict';
module.exports = (sequelize, DataTypes) => {
	const User = sequelize.define(
		'User',
		{
			email: {
				type: DataTypes.STRING(60),
				allowNull: false,
				unique: true,
				validate: {
					isEmail: true,
					len: [5, 60],
				},
			},
			firstName: {
				type: DataTypes.STRING(30),
				allowNull: false,
				field: 'first_name',
				validate: {
					len: [2, 30],
				},
			},
			lastName: {
				type: DataTypes.STRING(30),
				allowNull: false,
				field: 'last_name',
				validate: {
					len: [2, 30],
				},
			},
			password: DataTypes.STRING(60),
			startingPosition: {
				type: DataTypes.GEOMETRY('POINT'),
				field: 'starting_position',
				allowNull: true,
			},
		},
		{
			underscored: true,
			tableName: 'users',
		},
	);
	User.associate = function (models) {
		User.belongsToMany(models.Event, {
			as: 'Participations',
			through: models.Participant,
		});

		User.belongsToMany(models.Route, {
			as: 'FavouriteRoutes',
			through: models.FavouriteRoute,
		});

		User.belongsToMany(models.User, {
			as: 'FavouriteUsers',
			through: models.FavouriteUser,
			otherKey: 'friend_id',
		});

		User.hasMany(models.Event, {
			as: 'Events',
		});

		User.hasMany(models.Route, {
			as: 'Routes',
		});

		User.hasMany(models.Run);

		User.hasMany(models.Comment);
	};
	return User;
};
