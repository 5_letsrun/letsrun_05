import expressAsyncAwait from 'express-async-await';
import { Router } from 'express';

import { loginUser, signUpUser } from './authController';

const router = expressAsyncAwait(Router());
router.post('/login', loginUser);
router.post('/signUp', signUpUser);

export default router;
