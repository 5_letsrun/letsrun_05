'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		const favouriteRoutesPromise = queryInterface.bulkInsert(
			'favourite_routes',
			[
				{
					route_id: 1,
					user_id: 1,
					created_at: new Date(),
					updated_at: new Date(),
				},
			],
			{},
		);

		return Promise.all([favouriteRoutesPromise]);
	},

	down: (queryInterface, Sequelize) => {
		return Promise.all([
			queryInterface.bulkDelete('favourite_routes', null, {}),
		]);
	},
};
