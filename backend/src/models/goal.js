'use strict';
module.exports = (sequelize, DataTypes) => {
	const Goal = sequelize.define(
		'Goal',
		{
			year: {
				type: DataTypes.SMALLINT.UNSIGNED,
				allowNull: false,
				unique: 'composite',
				validate: {
					isInt: true,
					min: 2018,
					max: 2099,
				},
			},
			month: {
				type: DataTypes.TINYINT.UNSIGNED,
				allowNull: false,
				unique: 'composite',
				validate: {
					isInt: true,
					min: 1,
					max: 12,
				},
			},
			amount: {
				type: DataTypes.DECIMAL(8, 2).UNSIGNED,
				allowNull: false,
				validate: {
					isDecimal: true,
				},
			},
			user_id: {
				type: DataTypes.INTEGER(10).UNSIGNED,
				allowNull: false,
				unique: 'composite',
			},
			// indexes: [
			//     { unique: 'composite', fields: ['user_id'] }
			// ]
		},
		{
			underscored: true,
			tableName: 'goals',
		},
	);
	Goal.associate = function(models) {
		Goal.belongsTo(models.User);
	};
	return Goal;
};
