'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface
			.createTable('favourite_routes', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				route_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				user_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				created_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updated_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			})
			.then(() => {
				queryInterface.addConstraint('favourite_routes', ['route_id'], {
					type: 'foreign key',
					references: {
						table: 'routes',
						field: 'id',
					},
				});

				queryInterface.addConstraint('favourite_routes', ['user_id'], {
					type: 'foreign key',
					references: {
						table: 'users',
						field: 'id',
					},
					onDelete: 'cascade',
				});

				queryInterface.addIndex(
					'favourite_routes',
					['route_id', 'user_id'],
					{
						unique: true,
					},
				);
			});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('favourite_routes');
	},
};
