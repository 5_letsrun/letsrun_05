import shortid from 'shortid';

export const ADD_FLASH_MESSAGE = 'ADD_FLASH_MESSAGE';
export const REMOVE_FLASH_MESSAGE = 'REMOVE_FLASH_MESSAGE';
export const REMOVE_ALL_FLASH_MESSAGES = 'REMOVE_ALL_FLASH_MESSAGES';

export const createFlashMessage = payload => {
	return dispatch => {
		const flashId = shortid.generate();
		payload.id = flashId;
		dispatch(addFlashMessage(payload));
		setTimeout(() => {
			dispatch(removeFlashMessage({flashId}))
		}, 3000);
	};
};

export const addFlashMessage = payload => {
	return {
		type: ADD_FLASH_MESSAGE,
		payload,
	};
};

export const removeFlashMessage = payload => {
	return {
		type: REMOVE_FLASH_MESSAGE,
		payload,
	};
};

export const removeAllFlashMessages = () => {
	return {
		type: REMOVE_ALL_FLASH_MESSAGES,
	};
};
