import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Layout from '../components/Layout/Layout';
import Homepage from '../scenes/Homepage/Homepage';
import Login from '../scenes/Login/Login';
import SignUp from '../scenes/Signup/Signup';
import EventDetail from '../scenes/EventDetail/EventDetail';
import NewEvent from '../scenes/NewEvent/NewEvent';
import Statistics from '../scenes/Statistics/Statistics';
import UserSetting from '../scenes/UserSetting/UserSetting';
import Logout from '../scenes/Logout/Logout';
import FavouriteUsers from '../scenes/FavouriteUsers/FavouriteUsers';

import { autoCheckState } from '../services/Auth/authActions';

class AppRoutes extends Component {
	componentDidMount() {
		this.props.onTryAutoLogin();
	}

	render() {
		const { isLoggedIn } = this.props;
		let routes = null;

		if (isLoggedIn) {
			routes = (
				<Layout>
					<Switch>
						<Route path="/" exact component={Homepage} />
						<Route
							path="/event/:id"
							exact
							component={EventDetail}
						/>
						<Route path="/newEvent" exact component={NewEvent} />
						<Route
							path="/statistics/:userId"
							exact
							component={Statistics}
						/>
						<Route
							path="/favouriteUsers/:userId"
							exact
							component={FavouriteUsers}
						/>
						<Route path="/logout" exact component={Logout} />
						<Route
							path="/userSetting"
							exact
							component={UserSetting}
						/>
						<Redirect to="/" />
					</Switch>
				</Layout>
			);
		} else {
			routes = (
				<Switch>
					<Route path="/signup" exact component={SignUp} />
					<Route path="/" component={Login} />
				</Switch>
			);
		}

		return routes;
	}
}

const mapStateToProps = state => {
	const { token } = state.auth;

	return {
		isLoggedIn: token,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onTryAutoLogin: () => dispatch(autoCheckState()),
	};
};

export default withRouter(
	connect(
		mapStateToProps,
		mapDispatchToProps,
	)(AppRoutes),
);
