import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setTime } from '../../services/Recorder/actions';
import FontIcon from '../FontIcon/FontIcon';

import './Recorder.css';


const format = time =>
	digits(time / 3600000) +
	':' +
	digits(time / 60000) +
	':' +
	digits((time % 60000) / 1000);
const digits = number => {
	number = Math.floor(number);
	return (number < 10 ? '0' : '') + number;
};

class Recorder extends Component {
	constructor(props) {
		super(props);

		this.clock = null;
		this.start = 0;

		this.state = {
			time: 0,
			text: '00:00,00',
			started: false,
			stopped: false,
			last: this.props.last,
			best: this.props.best,
			target: this.props.target,
		};
	}

	tick = () => {
		let now = new Date();
		let time = now * 1 - this.start * 1;
		this.setState({ time: time, text: format(time) });
	};

	click = () => {
		let { started, stopped, last, best, target, time, text } = this.state;
		if (!started) {
			started = true;
			this.start = new Date();
			this.clock = setInterval(this.tick, 10);
		} else {
			if (!stopped) {
				stopped = true;
				clearInterval(this.clock);
				/*
        TODO: Tady zavolat SAVE!!!
        */
			} else {
				started = false;
				stopped = false;
				last = time;
				this.props.target === undefined ? (target = 0.95 * last) : 0;
				best === undefined || best > time ? (best = time) : 0;
				time = 0;
				text = format(time);
			}
			this.props.setTime(text);
		}
		this.setState({
			started: started,
			stopped: stopped,
			last: last,
			best: best,
			target: target,
			time: time,
			text: text,
		});
	};

	render() {
		const { started, stopped, text} = this.state;
		return (
			<div className="recorder">
				<p className="stopwatch-title"><FontIcon icon="stopwatch" /> Stopwatch</p>
				<hr className="my-hr"/>
				<div className="recorderTime col">{text}</div>
				<hr className="my-hr"/>
				<div className="text-center">
					<button
						className='recorderButton text-uppercase btn btn-info btn-lg btn-lg'
						onClick={this.click}
					>
						{' '}
						{!started
							? 'START'
							: !stopped
							? 'STOP'
							: 'RESET'}{' '}
					</button>
				</div>
			</div>
		);
	}
}

const mapDispatchToProps = {
	setTime,
};

export default connect(
	null,
	mapDispatchToProps,
)(Recorder);
