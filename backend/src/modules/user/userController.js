import db from '../../models';
import bcrypt from 'bcrypt';

import { GENERAL_ERROR } from '../auth/authController';

const USER_NOT_FOUND = 'User not found.';

const User = db.User;
const Participant = db.Participant;
const FavouriteUser = db.FavouriteUser;

export const getUserByEmail = async email => {
	return await User.findOne({
		where: {
			email,
		},
	});
};

export const getUserById = async id => {
	return await User.findByPk(id);
};

export const getAllUsers = async (req, res) => {
	const users = await User.findAll();
	res.status(200).json({ users });
};

export const createUser = ({
	email,
	firstName,
	lastName,
	password,
	lat,
	lng,
}) => {
	const startingPosition =
		lat && lng ? { type: 'Point', coordinates: [lat, lng] } : null;

	return User.create({
		email,
		firstName,
		lastName,
		password: bcrypt.hashSync(password, 8),
		startingPosition,
	});
};

export const editUser = async (req, res) => {
	const { id, firstName, lastName, lat, lng } = req.body;
	const user = await getUserById(id);

	if (user) {
		const startingPosition =
			lat && lng ? { type: 'Point', coordinates: [lat, lng] } : null;
		user.update({
			firstName,
			lastName,
			startingPosition,
		})
			.then(user => {
				const {
					id,
					email,
					firstName,
					lastName,
					startingPosition: position,
				} = user;
				const { coordinates } = position || {};
				const startingPosition = coordinates
					? {
							lat: coordinates[0],
							lng: coordinates[1],
					  }
					: null;
				res.status(200).json({
					user: {
						id,
						email,
						firstName,
						lastName,
						startingPosition,
					},
				});
			})
			.catch(error => {
				res.status(400).json({ error: GENERAL_ERROR });
			});
	} else {
		res.status(404).json({ error: USER_NOT_FOUND, form: req.body });
	}
};

export const isUserRegisteredForEvent = async (req, res) => {
	const user = req.query.userId;
	const event = req.query.eventId;
	let participant;

	participant = await Participant.findOne({
		where: {
			userId: user,
			eventId: event,
		},
	});

	if (participant != null) {
		return res.status(200).json({ isUserRegisteredForEvent: true });
	} else return res.status(200).json({ isUserRegisteredForEvent: false });
};

export const getFavouriteUsers = async (req, res) => {
	const favouriteUsers = await FavouriteUser.findAll({
		where: {
			user_id: req.param('id'),
		},
	});

	let favouriteUsersId = favouriteUsers.map(({ friend_id }) => friend_id);

	let users = await User.findAll({
		where: {
			id: [favouriteUsersId],
		},
	});
	console.log(users);
	res.status(200).json({ favouriteUsers, users });
};

export const addToFavouriteUsers = async (req, res) => {
	await User.findById(req.param('id')).then(user => {
		user.addFavouriteUsers(req.param('friendId'));
	});

	res.status(201).send();
};

export const removeFromFavouriteUsers = async (req, res) => {
	await User.findById(req.param('id')).then(user => {
		user.removeFavouriteUsers(req.param('friendId'));
	});

	res.status(204).send();
};

export const getUserName = async (req, res) => {
	var userId = req.query.userId;
	var user = null;
	console.log(userId);

	user = await User.findOne({
		where: {
			id: userId,
		},
	});

	if (user != null) {
		let userName = user.firstName + ' ' + user.lastName;
		return res.status(200).json({ userName: userName });
	}
};
