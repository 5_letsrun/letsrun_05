import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import Item from './MenuItem/MenuItem';
import Logo from './Logo/index';
import UserSetting from './UserSetting/UserSetting';
import Weather from './Weather/Weather';

import './Navigation.css';

class Navigation extends Component {
	render() {
		return (
			<nav
				id="toolbar"
				className="nav navbar navbar-expand-lg navbar-light"
			>
				<button
					className="navbar-toggler"
					type="button"
					data-toggle="collapse"
					data-target="#navbarToggler"
					aria-controls="navbarToggler"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span className="navbar-toggler-icon" />
				</button>
				<div
					className="nav navbar navbar-expand-lg navbar-light"
					id="logo"
				>
					<ul className="nav navbar-nav flex-fill justify-content-start">
						<Item path="/">
							<Logo />
						</Item>
					</ul>
				</div>
				<div
					className="nav navbar navbar-expand-lg navbar-light"
					id="weather"
				>
					<ul className="nav navbar-nav flex-fill justify-content-start">
						<Item classNames="weather-container">
							<Weather />
						</Item>
					</ul>
				</div>
				<div className="collapse navbar-collapse" id="navbarToggler">
					<ul className="nav navbar-nav menu-items" id="navbarItems">
						<Item path="/">Dashboard</Item>
						<Item path={'/statistics/' + this.props.userId}>
							Statistics
						</Item>
						<Item path="/newEvent">New Event</Item>
						<Item path={'/favouriteUsers/' + this.props.userId}>
							Favourite Users
						</Item>
					</ul>
				</div>
				<div
					className="nav navbar navbar-expand-lg navbar-light"
					id="logOut"
				>
					<ul className="nav navbar-nav flex-fill w-100 justify-content-end">
						<Item>
							<UserSetting />
						</Item>
					</ul>
				</div>
			</nav>
		);
	}
}

const mapStateToProps = state => {
	const { id: userId } = state.auth;
	return {
		userId,
	};
};

export default withRouter(
	connect(
		mapStateToProps,
		null,
	)(Navigation),
);
