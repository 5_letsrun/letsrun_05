import jwt from 'jsonwebtoken';

export const INVALID_TOKEN = 'Invalid Token. Please login again.';

export const getSecret = () => process.env.SECRET;

export const createToken = id =>  jwt.sign({ id }, getSecret(), { expiresIn: 86400 });

export const verifyToken = (req, res, next) => {
	const token = req.headers['x-letsrun-access-token'];
	jwt.verify(token, getSecret(), (error) => {
		if (error) {
			return res.status(401).send({
				error: INVALID_TOKEN,
			});
		}
		next();
	});
};
