'use strict';
module.exports = (sequelize, DataTypes) => {
	const Route = sequelize.define(
		'Route',
		{
			start: {
				type: DataTypes.GEOMETRY('POINT'),
				allowNull: false,
			},
			trace: {
				type: DataTypes.JSON,
				allowNull: false,
			},
			distance: {
				type: DataTypes.DECIMAL(8, 2).UNSIGNED,
				allowNull: false,
				validate: {
					isDecimal: true,
				},
			},
			userId: {
				type: DataTypes.INTEGER.UNSIGNED,
				allowNull: false,
				field: 'user_id',
			},
		},
		{
			underscored: true,
			tableName: 'routes',
		},
	);
	Route.associate = function(models) {
		Route.belongsTo(models.User);

		Route.belongsToMany(models.User, {
			as: 'FavouriteRoutes',
			through: models.FavouriteRoute,
		});

		Route.hasMany(models.Event);
	};
	return Route;
};
