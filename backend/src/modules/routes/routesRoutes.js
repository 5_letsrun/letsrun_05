import expressAsyncAwait from 'express-async-await';
import { Router } from 'express';

import { getAllRoutes, getRoute, setRoute, addFavouriteRoutes, getFavouriteRoutes, deleteFavouriteRoute } from './routesController';

const router = expressAsyncAwait(Router());

router.get('/', getAllRoutes);
router.get('/:id', getRoute);
router.post('/', setRoute);
router.post('/favorite/:id', addFavouriteRoutes);
router.get('/favorite/:id', getFavouriteRoutes);
router.post('/delete-favourite', deleteFavouriteRoute);

export default router;
