import React from 'react';
import EventHalfDetail from '../EventHalfDetail/EventHalfDetail.js';

import './EventItem.css';

export const EventItem = ({ event }) => {
	const { id, date } = event.Event;
	const distance = event.Event.Route.distance;
	const name = event.Event.name;
	var eventItemHref = '#eventItem' + id;
	var eventItemId = 'eventItem' + id;
	var eventId = event.Event.id;
	var eventPath = '/event/' + eventId;
	var routeId = event.Event.Route.id;

	return (
		<div id="accordion">
			<a
				className="container rounded eventitem btn"
				data-toggle="collapse"
				type="button"
				aria-expanded="true"
				aria-controls="collapseExample"
				id={event.id}
				href={eventItemHref}
			>
				<div className="row">
					<div className="col text-truncate">
						<label>{name}</label>
					</div>
					<div className="col">
						<label>{date}</label>
					</div>
					<div className="col">
						<label>{distance} km</label>
					</div>
					<div className="col">
						<label>Not run yet</label>
					</div>
				</div>
			</a>

			<div id={eventItemId} className="collapse" data-parent="#accordion">
				<br />
				<a className="btn btn-info" href={eventPath}>
					Open event detail
				</a>
				{<EventHalfDetail id={event.id} routeId={routeId} />}
			</div>
		</div>
	);
};
