import React, { Component } from 'react';
import { MonthPicker } from 'react-dropdown-date';
import { connect } from 'react-redux';
import { changeMonth } from '../../services/Statistics/actions';

class MonthDropdown extends Component {
	constructor(props) {
		super(props);
		const date = new Date();
		const month = date.getMonth();

		this.props = { year: null, month: month, day: null };
	}

	render() {
		const { changeMonth } = this.props;

		return (
			<span id="MonthDropdown">
				<label>Měsíc</label>
				<MonthPicker
					required={true}
					value={this.props.month}
					onChange={changeMonth}
					id={'MonthPicker'}
				/>
			</span>
		);
	}
}

const mapStateToProps = state => {
	return {
		month: state.month,
	};
};

const mapDispatchToProps = {
	changeMonth,
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(MonthDropdown);
