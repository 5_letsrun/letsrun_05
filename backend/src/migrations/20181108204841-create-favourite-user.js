'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface
			.createTable('favourite_users', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				user_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				friend_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				created_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updated_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			})
			.then(() => {
				queryInterface.addConstraint('favourite_users', ['user_id'], {
					type: 'foreign key',
					references: {
						table: 'users',
						field: 'id',
					},
					onDelete: 'cascade',
				});

				queryInterface.addConstraint('favourite_users', ['friend_id'], {
					type: 'foreign key',
					references: {
						table: 'users',
						field: 'id',
					},
					onDelete: 'cascade',
				});

				queryInterface.addIndex(
					'favourite_users',
					['user_id', 'friend_id'],
					{
						unique: true,
					},
				);
			});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('favourite_users');
	},
};
