export const ROUTES_SELECT_ROUTE = 'ROUTES_SELECT_ROUTE';

export const selectRoute = ( distance, userId, start, trace ) => ({
  type: ROUTES_SELECT_ROUTE,
  payload: { distance, userId, start, trace },
});
