import React, { Component } from 'react';

import './ListHeader.css';

export class ListHeader extends Component {
	render() {
		return (
			<div className="container statisticsheading">
				<div className="row statisticsheader">
					<div className="col statisticsheader">
						<label className="headinglabel">Event</label>
					</div>
					<div className="col statisticsheader">
						<label className="headinglabel">Date</label>
					</div>
					<div className="col statisticsheader">
						<label className="headinglabel">Distance</label>
					</div>
					<div className="col statisticsheader">
						<label className="headinglabel">Time</label>
					</div>
				</div>
			</div>
		);
	}
}
