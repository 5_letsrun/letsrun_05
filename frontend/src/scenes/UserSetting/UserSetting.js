import React from 'react';
import {connect} from 'react-redux';

import {processUpdateUser} from "../../services/Auth/authActions";
import {IS_EMPTY, Validator} from "../../utils/Validator/Validator";
import Modal from "../../components/Modal/Modal";
import FontIcon from "../../components/FontIcon/FontIcon";
import PositionPicker from "../../components/PositionPicker/PositionPicker";
import PageTitle from '../../components/PageTitle/PageTitle';

class UserSetting extends React.Component {
	state = {
		form: {
			firstName: '',
			lastName: '',
			lat: '',
			lng: '',
		},
		errorMessage: '',
	};

	validator = new Validator([
		{
			field: 'firstName',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'First name cannot be empty.',
				},
			],
		},
		{
			field: 'lastName',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'Last name cannot be empty.',
				},
			],
		},
	]);

	componentDidMount() {
		const {firstName, lastName, startingPosition} = this.props;
		const {lat = '', lng = ''} = startingPosition || {};
		const form = {
			...this.state.form,
			firstName,
			lastName,
			lat,
			lng,
		};
		this.setState({
			form,
		});
	}

	onChangeHandler = event => {
		const fieldName = event.target.name;
		const value = event.target.value;
		const form = {
			...this.state.form,
			[fieldName]: value,
		};
		this.setState({form});
	};

	onSubmitHandler = event => {
		event.preventDefault();

		const form = {
			...this.state.form,
			submitted: true,
		};
		this.setState({form});
		if (this.validator.validate(form)) {
			const {firstName, lastName, lat, lng} = form;
			const {id} = this.props;
			this.props.onSubmit({
				id,
				firstName,
				lastName,
				lat,
				lng,
			});
		}
	};

	onMapClickHandler = ({lat, lng}) => {
		const form = {
			...this.state.form,
			lat,
			lng,
		};
		this.setState({form});
	};

	render() {
		const {
			firstName,
			lastName,
			lat,
			lng
		} = this.state.form;

		return (
			<div id="user-setting">
				<PageTitle>Profile</PageTitle>
				<div className="my-jumbotron">
					<form onSubmit={this.onSubmitHandler}>
						<div className="form-group row">
							<label htmlFor="firstName" className="col-sm-2 col-form-label">First name</label>
							<div className="col-sm-4">
								<input
									type="text"
									className={this.validator.getClassNames(
										'firstName',
									)}
									value={firstName}
									name="firstName"
									onChange={this.onChangeHandler}
								/>
								<span className="help-block">
								{this.validator.getErrorMessage(
									'firstName',
								)}
							</span>
							</div>
						</div>
						<div className="form-group row">
							<label htmlFor="lastName" className="col-sm-2 col-form-label">Last name</label>
							<div className="col-sm-4">
								<input
									type="text"
									className={this.validator.getClassNames(
										'lastName',
									)}
									value={lastName}
									name="lastName"
									onChange={this.onChangeHandler}
								/>
								<span className="help-block">
								{this.validator.getErrorMessage('lastName')}
							</span>
							</div>
						</div>
						<div className="form-group row">
							<label className="col-sm-2 col-form-label">Starting position</label>
							<div className="col-sm-1" style={{position: 'static'}}>
								<Modal
									btnTrigger={<FontIcon icon="map-marker-alt"/>}
									btnSubmit="Choose this coordinate"
									title="Find your starting position!"
								>
									<PositionPicker onClick={this.onMapClickHandler}/>
								</Modal>
							</div>
							<div className="col-sm-2">
								<div className="form-group">
									<label htmlFor="lat">Lat</label>
									<input
										type="text"
										className="form-control"
										value={lat}
										name="lat"
										readOnly
									/>
								</div>
								<div className="form-group">
									<label htmlFor="lng">Lng</label>
									<input
										type="text"
										className="form-control"
										value={lng}
										name="lng"
										readOnly
									/>
								</div>
							</div>
						</div>
						<button
							type="submit"
							className="btn btn-info btn-block"
						>
							Edit
						</button>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const {id, firstName, lastName, startingPosition} = state.auth;
	return {
		id,
		firstName,
		lastName,
		startingPosition,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onSubmit: payload => dispatch(processUpdateUser(payload)),
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(UserSetting);
