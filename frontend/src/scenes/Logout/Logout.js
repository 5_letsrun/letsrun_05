import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { processLogout } from '../../services/Auth/authActions';
import { createFlashMessage } from '../../services/Share/shareActions';

class Logout extends Component {
	componentDidMount() {
		this.props.onLogout();
		this.props.history.push('/login');
	}

	render() {
		return <Redirect to="/login" />;
	}
}

const mapDispatchToProps = dispatch => {
	return {
		addFlashMessage: payload => dispatch(createFlashMessage(payload)),
		onLogout: () => dispatch(processLogout()),
	};
};

export default connect(
	null,
	mapDispatchToProps,
)(Logout);
