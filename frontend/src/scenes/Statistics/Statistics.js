import React, { Component } from 'react';
import { connect } from 'react-redux';
import Axios from '../../axios';

import DatePicker from '../../components/DatePickers/DatePicker';
import RunItemList from '../../components/Lists/RunItemList';
import MonthGoal from '../../components/MonthGoal/MonthGoal';
import { changeMonth } from '../../services/Statistics/actions';
import { changeYear } from '../../services/Statistics/actions';
import PageTitle from '../../components/PageTitle/PageTitle';

class Statistics extends Component {
	state = {
		userName: '',
	};

	componentDidMount() {
		Axios.get(`/user/getUserName`, {
			params: {
				userId: this.props.match.params.userId,
			},
		}).then(response => {
			let userName = response.data.userName;

			this.setState({
				userName: userName,
			});
		});
	}
	render() {
		let pageTitle;
		if (this.props.match.params.userId === this.props.userId) {
			pageTitle = 'My Statitics';
		} else {
			pageTitle = 'Statistics - ' + this.state.userName;
		}

		return (
			<div id="event-container" className="event-container">
				<PageTitle>{pageTitle}</PageTitle>

				<ul className="date-container my-jumbotron mb-2">
					<DatePicker
						statisticsUser={this.props.match.params.userId}
					/>
				</ul>
				<ul className="date-container my-jumbotron mb-2">
					<MonthGoal
						statisticsUser={this.props.match.params.userId}
					/>
				</ul>
				<div className="date-container my-jumbotron mb-2">
					<RunItemList
						statisticsUser={this.props.match.params.userId}
					/>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return { userId: state.auth.id };
};

const mapDispatchToProps = {
	changeMonth,
	changeYear,
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Statistics);
