'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface
			.createTable('runs', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				time: {
					allowNull: false,
					type: Sequelize.TIME,
				},
				event_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				user_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				created_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updated_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			})
			.then(() => {
				queryInterface.addConstraint('runs', ['event_id'], {
					type: 'foreign key',
					references: {
						table: 'events',
						field: 'id',
					},
				});

				queryInterface.addConstraint('runs', ['user_id'], {
					type: 'foreign key',
					references: {
						table: 'users',
						field: 'id',
					},
					onDelete: 'cascade',
				});

				queryInterface.addIndex('runs', ['event_id', 'user_id'], {
					unique: true,
				});
			});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('runs');
	},
};
