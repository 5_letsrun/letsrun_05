import { ROUTES_SELECT_ROUTE } from './actions';

const initialState = {
  distance: '',
  userId: '',
  start: '',
  trace: [],

};

export const routesReducer = (state = initialState, action) => {
  switch(action.type) {
    case ROUTES_SELECT_ROUTE: {
      return { ...state,
        distance: action.payload.distance,
        userId: action.payload.userId,
        start: action.payload.start,
        trace: action.payload.trace,
      };
    }

    default:
      return state;
  }
};
