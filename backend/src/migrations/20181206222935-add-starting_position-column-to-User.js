'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.addColumn(
			'users',
			'starting_position',
			{
				allowNull: true,
				type: Sequelize.GEOMETRY('POINT'),
			}
		);
	},

	down: (queryInterface) => {
		return queryInterface.removeColumn(
			'users',
			'starting_position'
		);
	}
};
