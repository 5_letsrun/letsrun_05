import {processLogout} from "./Auth/authActions";

export default (axios, store) => {
	axios.interceptors.request.use(req => {
		const token = localStorage.getItem('token');
		if (token) {
			req.headers['x-letsrun-access-token'] = token;
		}

		return req;
	});

	axios.interceptors.response.use(res => res, errorResponse => {
		if (errorResponse.response.status === 401) {
			const {error} = errorResponse.response.data;
			store.dispatch(processLogout({error, redirect: true}));
		}
		return Promise.reject(errorResponse);
	});
};
