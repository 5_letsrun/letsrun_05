'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('users', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER.UNSIGNED,
			},
			email: {
				allowNull: false,
				unique: true,
				type: Sequelize.STRING(60),
			},
			first_name: {
				allowNull: false,
				type: Sequelize.STRING(30),
			},
			last_name: {
				allowNull: false,
				type: Sequelize.STRING(30),
			},
			full_name: {
				allowNull: false,
				type: Sequelize.STRING(60),
			},
			password: {
				allowNull: false,
				type: Sequelize.STRING(60),
			},
			created_at: {
				allowNull: false,
				type: Sequelize.DATE,
			},
			updated_at: {
				allowNull: false,
				type: Sequelize.DATE,
			},
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('users');
	},
};
