import expressAsyncAwait from 'express-async-await';
import { Router } from 'express';

import { getAllEvents } from './eventController';
// import { getFavouriteEvents } from './eventController';
import { getEventDetail } from './eventController';
import { addComment } from './eventController';
import { participate } from './eventController';
import { newEvent } from './eventController';
import { isEventCompleteByUser } from './eventController';

const router = expressAsyncAwait(Router());
router.get('/', getAllEvents);
// router.get('/favourites', getFavouriteEvents);
router.get('/isEventCompleteByUser', isEventCompleteByUser);
router.post('/participants', participate);
router.post('/addComment', addComment);
router.get('/:id', getEventDetail);
router.post('/create', newEvent);

export default router;
