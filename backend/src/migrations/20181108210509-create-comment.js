'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface
			.createTable('comments', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				content: {
					allowNull: false,
					type: Sequelize.TEXT,
				},
				event_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				user_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				created_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updated_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			})
			.then(() => {
				queryInterface.addConstraint('comments', ['event_id'], {
					type: 'foreign key',
					references: {
						table: 'events',
						field: 'id',
					},
					onDelete: 'cascade',
				});

				queryInterface.addConstraint('comments', ['user_id'], {
					type: 'foreign key',
					references: {
						table: 'users',
						field: 'id',
					},
					onDelete: 'cascade',
				});
			});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('comments');
	},
};
