import React from 'react';
import ReactDOM from 'react-dom';
import { unregister } from './registerServiceWorker';

import './index.css';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
unregister();
