import db from '../../models';

const Run = db.Run;
const Event = db.Event;
const Route = db.Route;
const Participant = db.Participant;
const User = db.User;

export const getRuns = async (selectedMonth, selectedYear, user) => {
	var runs = await Run.findAll({
		where: {
			$and: [
				db.sequelize.where(
					db.sequelize.fn(
						'MONTH',
						db.sequelize.col('Run.created_at'),
					),
					selectedMonth,
				),
				db.sequelize.where(
					db.sequelize.fn('YEAR', db.sequelize.col('Run.created_at')),
					selectedYear,
				),
			],
			user_id: user,
		},
		attributes: [
			'id',
			'created_at',
			[
				db.sequelize.fn(
					'date_format',
					db.sequelize.col('Run.created_at'),
					'%d.%m.%Y',
				),
				'date',
			],
			'time',
		],
		include: [
			{
				model: Event,
				attributes: ['name', 'id'],
				include: [
					{
						model: Route,
						attributes: ['distance', 'trace', 'start', 'id'],
					},
				],
			},
		],
	});

	if (runs != null) {
		runs.sort(function(a, b) {
			return new Date(b.created_at) - new Date(a.created_at);
		});
	} else runs = null;

	return runs;
};

export const getEvents = async (selectedMonth, selectedYear, user) => {
	var events = await Participant.findAll({
		where: {
			userId: user,

			$and: [
				db.sequelize.where(
					db.sequelize.fn('MONTH', db.sequelize.col('Event.term')),
					selectedMonth,
				),
				db.sequelize.where(
					db.sequelize.fn('YEAR', db.sequelize.col('Event.term')),
					selectedYear,
				),
			],
		},
		// attributes: [
		// 	'id',
		// 	[
		// 		db.sequelize.fn(
		// 			'date_format',
		// 			db.sequelize.col('Event.term'),
		// 			'%d.%m.%Y',
		// 		),
		// 		'date',
		// 	],
		// 	'name',
		// ],

		include: [
			{
				model: Event,
				attributes: [
					'id',
					[
						db.sequelize.fn(
							'date_format',
							db.sequelize.col('Event.term'),
							'%d.%m.%Y',
						),
						'date',
					],
					'name',
				],
				include: [
					{
						model: Route,
						attributes: ['distance', 'trace', 'start', 'id'],
					},
				],
			},
		],

		// include: [
		// 	{
		// 		model: Route,
		// 		attributes: ['distance', 'trace', 'start', 'id'],
		// 	},
		// 	{ model: User, required: true },
		// ],
	});
	if (events != null) {
		events.sort(function(a, b) {
			return new Date(b.Event.date) - new Date(a.Event.date);
		});
	} else events = null;

	return events;
};

export const getStatisticsData = async (req, res) => {
	var selectedMonth = Number(req.query.month);
	var selectedYear = Number(req.query.year);
	var user = req.query.user;
	var events = await getEvents(selectedMonth, selectedYear, user);
	var runs = await getRuns(selectedMonth, selectedYear, user);
	console.log(events);
	console.log(runs);

	if (events != null && runs != null) {
		for (var i = 0; i < runs.length; i++) {
			for (var j = 0; j < events.length; j++) {
				if (events[j].Event.id == runs[i].Event.id) {
					events.splice(j, 1);
					break;
				}
			}
		}
	}

	return res.json({
		runs: runs,
		events: events,
	});
};
