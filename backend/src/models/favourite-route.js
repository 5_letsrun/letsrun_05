'use strict';
module.exports = (sequelize, DataTypes) => {
	const FavouriteRoute = sequelize.define(
		'FavouriteRoute',
		{
			name: {
				type: DataTypes.STRING(60),
				field: 'name',
			}
		},
		{
			underscored: true,
			tableName: 'favourite_routes',
		},
	);
	FavouriteRoute.associate = function(models) {
		FavouriteRoute.belongsTo(models.Route);
		FavouriteRoute.belongsTo(models.User);
	};
	return FavouriteRoute;
};
