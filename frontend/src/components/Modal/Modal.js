import React, {Component} from 'react';
import Aux from '../hoc/Auxi/Auxi';
import $ from 'jquery';

import './Modal.css'

class Modal extends Component {
	state = {
		show: false,
	};

	constructor(props) {
		super(props);
		this.modalRef = React.createRef();
	}

	componentDidMount() {
		const {current: modal} = this.modalRef;
		$(modal).appendTo('body').on('hidden.bs.modal', () => {
			this.setState({
				show: false,
			})
		});
	}

	componentDidUpdate() {
		const {show} = this.state;
		const {current: modal} = this.modalRef;
		if (show) {
			$(modal).modal('show');
		}
	}

	onClickHandler = () => {
		this.setState({
			show: true
		});
	};

	render () {
		const {
			btnTrigger,
			btnSubmit,
			title,
			children
		} = this.props;

		return (
			<Aux>
				<button onClick={this.onClickHandler} type="button" className="btn btn-info">
					{btnTrigger}
				</button>
				<div ref={this.modalRef} className="modal fade" tabIndex="-1" role="dialog"
					 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
					<div className="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title">{title}</h5>
								<button type="button" className="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div className="modal-body">
								{children}
							</div>
							<div className="modal-footer">
								<button type="button" className="btn btn-info btn-block" data-dismiss="modal">{btnSubmit}</button>
							</div>
						</div>
					</div>
				</div>
			</Aux>
		);
	}
}

export default Modal;
