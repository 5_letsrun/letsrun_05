'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		const eventsPromise = queryInterface.bulkInsert(
			'events',
			[
				{
					id: 1,
					name: 'Pražský půlmaraton',
					description: 'All runners are beautiful.',
					term: new Date(2019, 6, 15, 14, 30, 0),
					route_id: 1,
					user_id: 1,
					created_at: new Date(),
					updated_at: new Date(),
				},
				{
					id: 2,
					name: 'Sportisimo 1/2Maraton',
					description:
						'Sportisimo 1/2Maraton Praha je nádherný i po jednadvacáté. Nejen kvůli dechberoucím scenériím, ale hlavně kvůli jeho účastníkům.',
					term: new Date(2018, 12, 1, 12, 0, 0),
					route_id: 2,
					user_id: 2,
					created_at: new Date(),
					updated_at: new Date(),
				},
			],
			{},
		);

		return Promise.all([eventsPromise]);
	},

	down: (queryInterface, Sequelize) => {
		return Promise.all([queryInterface.bulkDelete('events', null, {})]);
	},
};
