import React, { Component } from 'react';
import './DatePicker.css';
import Axios from '../../axios';
import { setCurrentDistance } from '../../services/Statistics/actions.js';
import { connect } from 'react-redux';
import { changeMonth } from '../../services/Statistics/actions';
import { changeYear } from '../../services/Statistics/actions';
import { setGoal } from '../../services/Statistics/actions';

class DatePicker extends Component {
	constructor(props) {
		super(props);

		const date = new Date();
		const currentMonth = date.getMonth() + 1;
		const currentYear = date.getFullYear();

		this.state = {
			year: currentYear,
			month: currentMonth,
			user: this.props.user,
		};
	}

	handleChangeMonth = event => {
		var value = event.target.value;
		this.setState({ month: value });
	};

	handleChangeYear = event => {
		var value = event.target.value;
		this.setState({ year: value });
	};

	// handleSubmit = event => {
	// 	this.props.changeYear(this.state.year);
	// 	this.props.changeMonth(this.state.month);
	//
	// 	let currentDistance;
	// 	let goal;
	// 	let month = this.state.month;
	// 	let year = this.state.year;
	// 	Axios.get(`/statistics/currentDistance`, {
	// 		params: {
	// 			month: month,
	// 			year: year,
	// 			user: this.props.statisticsUser,
	// 		},
	// 	}).then(response => {
	// 		currentDistance = response.data.currentDistance;
	// 		// this.setState({
	// 		//   currentDistance: currentDistance
	// 		// });
	// 		this.props.setCurrentDistance(currentDistance);
	// 	});
	//
	// 	Axios.get(`/statistics/goal`, {
	// 		params: {
	// 			month: month,
	// 			year: year,
	// 			user: this.props.statisticsUser,
	// 		},
	// 	}).then(response => {
	// 		goal = response.data.goal;
	// 		this.props.setGoal(goal);
	// 	});
	// };

	componentDidUpdate() {
		this.props.changeYear(this.state.year);
		this.props.changeMonth(this.state.month);

		let currentDistance;
		let goal;
		let month = this.state.month;
		let year = this.state.year;
		Axios.get(`/statistics/currentDistance`, {
			params: {
				month: month,
				year: year,
				user: this.props.statisticsUser,
			},
		}).then(response => {
			currentDistance = response.data.currentDistance;
			// this.setState({
			//   currentDistance: currentDistance
			// });
			this.props.setCurrentDistance(currentDistance);
		});

		Axios.get(`/statistics/goal`, {
			params: {
				month: month,
				year: year,
				user: this.props.statisticsUser,
			},
		}).then(response => {
			goal = response.data.goal;
			this.props.setGoal(goal);
		});
	}

	componentDidMount() {
		this.props.changeYear(this.state.year);
		this.props.changeMonth(this.state.month);
	}

	render() {
		return (
			<div className="datepickerform container form-horizontal row">
				<div className="row">
					{/* <div className="row">	
					</div> */}
					<div className="col form-group-6">
						<label
							id="MonthDatePicker"
							className="date-label row-6"
						>
							Month
						</label>
						<select
							id="MonthDatePickerSelect"
							className="show-tick form-control datepicker row-6"
							data-live-search="true"
							onChange={this.handleChangeMonth}
							value={this.state.month}
						>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>
					</div>
					<div className="col form-group-6">
						<label
							id="YearDatePicker"
							className="date-label row-sm-6"
						>
							Year
						</label>
						<select
							id="YearDatePickerSelect"
							className="show-tick form-control datepicker row-sm-6"
							data-live-search="true"
							onChange={this.handleChangeYear}
							value={this.state.year}
						>
							<option value="2018">2018</option>
							<option value="2019">2019</option>
							<option value="2020">2020</option>
							<option value="2021">2021</option>
							<option value="2022">2022</option>
							<option value="2023">2023</option>
						</select>
						{/*<button
							id="DatePickerButton"
							className="datepickerbutton form-horizontal btn btn-primary col-sm-2"
							onClick={this.handleSubmit}
						>
							Filter
						</button>*/}
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		month: state.statistics.month,
		year: state.statistics.year,
		user: state.auth.id,
	};
};

const mapDispatchToProps = {
	changeMonth,
	changeYear,
	setCurrentDistance,
	setGoal,
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(DatePicker);
