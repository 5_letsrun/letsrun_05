import { RECORDER_SET_TIME } from './actions';

const initialState = {
	time: '',
};

export const recorderReducer = (state = initialState, action) => {
	switch (action.type) {
		case RECORDER_SET_TIME: {
			const { time } = action.payload;
			return { ...state, time: time };
		}

		default:
			return state;
	}
};
