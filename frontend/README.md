# Frontend
Frontend part of the Letsrun app. 

## Requirements
- [Node.js](https://nodejs.org/en/) v8.12.0 or later
- `yarn` (`npm install --global yarn`)

## Installation
```sh
yarn install
```

## Production build
```sh
yarn build
```

## Run local dev server
By default, [http://localhost:3000](http://localhost:3000) is opened.
```sh
yarn start
```

## Run prettier
```sh
yarn prettier
```