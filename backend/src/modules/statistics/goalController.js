import db from '../../models';

const Run = db.Run;
const Event = db.Event;
const Route = db.Route;
const Goal = db.Goal;

export const setGoal = async (req, res) => {
	var goal = req.body.goal;
	var user = req.body.user;
	var month = req.body.month;
	var year = req.body.year;

	var goal = await Goal.upsert({
		user_id: user,
		amount: goal,
		month: month,
		year: year,
	});
	return res.end(goal);
};

export const getGoal = async (req, res) => {
	var month = req.query.month;
	var year = req.query.year;
	var user = req.query.user;
	var currentGoal = await Goal.findOne({
		where: {
			year: year,
			month: month,
			user_id: user,
		},
	});
	if (currentGoal != null) {
		return res.json({ goal: currentGoal.amount });
	} else return res.json({ goal: 0 });
};

export const getCurrentDistance = async (req, res) => {
	var month = req.query.month;
	var year = req.query.year;
	var user = req.query.user;
	var runs = await Run.findAll({
		where: {
			$and: [
				db.sequelize.where(
					db.sequelize.fn(
						'MONTH',
						db.sequelize.col('Run.created_at'),
					),
					month,
				),
				db.sequelize.where(
					db.sequelize.fn('YEAR', db.sequelize.col('Run.created_at')),
					year,
				),
			],
			user_id: user,
		},
		attributes: [],
		include: [
			{
				model: Event,
				include: [
					{
						model: Route,
						attributes: ['distance'],
					},
				],
			},
		],
	});
	var currentDistance = 0;
	runs.forEach(function(run) {
		var distance = parseFloat(run.Event.Route.distance);
		currentDistance = currentDistance + distance;
	});

	return res.json({ currentDistance: currentDistance });
};
