import expressAsyncAwait from 'express-async-await';
import { Router } from 'express';

import { createRun } from './runController.js';

const router = expressAsyncAwait(Router());
router.post('/create', createRun);

export default router;
