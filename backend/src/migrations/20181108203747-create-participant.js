'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface
			.createTable('participants', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				event_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				user_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				created_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updated_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			})
			.then(() => {
				queryInterface.addConstraint('participants', ['event_id'], {
					type: 'foreign key',
					references: {
						table: 'events',
						field: 'id',
					},
					onDelete: 'cascade',
				});

				queryInterface.addConstraint('participants', ['user_id'], {
					type: 'foreign key',
					references: {
						table: 'users',
						field: 'id',
					},
					onDelete: 'cascade',
				});

				queryInterface.addIndex(
					'participants',
					['event_id', 'user_id'],
					{
						unique: true,
					},
				);
			});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('participants');
	},
};
