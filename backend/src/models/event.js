'use strict';
module.exports = (sequelize, DataTypes) => {
	const Event = sequelize.define(
		'Event',
		{
			name: {
				type: DataTypes.STRING(50),
				allowNull: false,
				validate: {
					len: [1, 50],
				},
			},
			description: {
				type: DataTypes.TEXT,
				allowNull: false,
			},
			term: {
				type: DataTypes.DATE,
				allowNull: false,
				validate: {
					isDate: true,
				},
			},
			/*routeId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      field: 'route_id'
    },
    userId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      field: 'user_id'
    }*/
		},
		{
			underscored: true,
			tableName: 'events',
		},
	);
	Event.associate = function(models) {
		Event.belongsTo(models.Route);

		Event.belongsTo(models.User);

		Event.belongsToMany(models.User, {
			as: 'Participants',
			through: models.Participant,
		});

		Event.hasMany(models.Comment);

		Event.hasMany(models.Run);
	};
	return Event;
};
