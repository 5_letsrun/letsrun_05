import React, { Component } from 'react';
import { connect } from 'react-redux';

import { selectRoute } from '../../services/Routes/actions';
import Axios from '../../axios';
import FavouriteButton from './FavouriteButton/FavouriteButton';

import './Map.css';


const MapTip = props => (
	<span className="mapTip">
		{props.text !== undefined && props.text !== '' ? 'TIP: ' : ''}
		{props.text}
	</span>
);

class Map extends Component {
	constructor(props) {
		super(props);

		this.GMAPI = null;
		this.mapRef = React.createRef();
		this.startMarker = null;
		this.endMarker = null;
		this.routeId = null;

		this.state = {
			center: (props.dynamic === true ? this.props.userCenter : this.props.center),
			points: props.points,
			length: '0 km',
			walk: '0 min',
			run: '0 min',
			estimated: 0,
			tip: '',
		};
	}

	createEndPoints = e => {
		const { points } = this.state;
		if (points === undefined || points.length < 2) {
			var GMAPI = this.GMAPI;
			let point = { lat: e.latLng.lat(), lng: e.latLng.lng() };
			let marker = new window.google.maps.Marker({
				position: point,
				label: {
					color: 'white',
					text: this.startMarker == null ? 'A' : 'B',
				},
				map: GMAPI.map,
				draggable: true,
			});

			if (this.startMarker == null) {
				this.startMarker = marker;
				this.setState({
					tip:
						'Now you can change the start point(by pulling it in the map) or set the route goal.',
				});
			} else {
				this.endMarker = marker;
				this.setState({
					points: [
						{
							lat: this.startMarker.position.lat(),
							lng: this.startMarker.position.lng(),
						},
						{
							lat: this.endMarker.position.lat(),
							lng: this.endMarker.position.lng(),
						},
					],
				});
				this.createTrace();
			}
		}
	};

	createTrace = () => {
		const { points } = this.state;
		var GMAPI = this.GMAPI;
		var that = this;
		if (points !== undefined && points.length > 1) {

			let wpts = points.slice(1, points.length - 1);
			let mwpts = [];
			for (var i = 0; i < wpts.length; i++)
			{
				mwpts.push({
					location: {
						lat: wpts[i].lat,
						lng: wpts[i].lng,
					},
					stopover: false
				});
			}

			GMAPI.service.route(
				{
					origin: points[0],
					waypoints: mwpts,
					destination: points[points.length - 1],
					travelMode: 'WALKING',
				},
				function(response, status) {
					if (status === 'OK') {
						if (that.startMarker != null) {
							that.startMarker.setVisible(false);
						}
						if (that.endMarker != null) {
							that.endMarker.setVisible(false);
						}
						that.setState({
							tip:
								'You can adjust the route by moving the destination or intersection points.',
						});
						GMAPI.renderer.setDirections(response);
						GMAPI.renderer.setMap(GMAPI.map);
					} else {
						GMAPI.renderer.setMap(null);
					}
				},
			);
		}
	};

	editTrace = () => {
		var GMAPI = this.GMAPI;
		let leg = GMAPI.renderer.getDirections().routes[0].legs[0];
		let runValue = leg.duration.value / 2.25;
		let runText =
			(runValue / 3600 >= 1
				? Math.floor(runValue / 3600) + ' hrs, '
				: '') +
			Math.round((runValue % 3600) / 60) +
			' min';
		var steps = leg.via_waypoint;
		var newPoints = [];
		newPoints.push({
			lat: leg.start_location.lat(),
			lng: leg.start_location.lng(),
		});
		for (var i = 0; i < steps.length; i++) {
			newPoints.push({
				lat: steps[i].location.lat(),
				lng: steps[i].location.lng(),
			});
		}
		newPoints.push({
			lat: leg.end_location.lat(),
			lng: leg.end_location.lng(),
		});
		this.setState({
			length: leg.distance.text,
			walk: leg.duration.text,
			run: runText,
			points: newPoints,
			estimated: runValue,
		});

		if (this.props.dynamic) {
			var data = {
				userId: localStorage.getItem('id'),
				start: newPoints[0],
				trace: newPoints,
				distance: leg.distance.value,
				walk: leg.duration.value,
				run: runValue,
			};
			if (this.routeId) {
				data.id = this.routeId;
			}
			this.output(data);

			this.props.selectRoute(
				data.distance,
				data.userId,
				data.start,
				data.trace,
			);
		}
	};

	init = () => {
		if (this.props.routeId)
		{
			this.routeId = this.props.routeId;
			Axios.get(`/routes/` + this.routeId).then(response => {
				let data = response.data.data;
				var points = [];
				try {
					points = JSON.parse(data.trace);
				} catch (e) {
					points = data.trace;
				}
				this.setState({ points: points, center: points[0] });
				this.mapInit();
			});
		}
		else
		{
			this.mapInit();
		}
	};

	mapInit = () => {
		const { current: mapDiv} = this.mapRef;
		const { points, center = { lat: 50.085343, lng: 14.435975 }  } = this.state;
		const { zoom = 14	} = this.props;
		if (this.GMAPI === null) {
			this.GMAPI = {
				map: new window.google.maps.Map(
					mapDiv,
					{
						zoom: zoom,
						center: center
					},
				),
				service: new window.google.maps.DirectionsService(),
				renderer: new window.google.maps.DirectionsRenderer({
					draggable: this.props.dynamic,
					preserveViewport: !this.props.dynamic,
					polylineOptions: {
						strokeColor: '#17a2b8',
						icons: [
							{
								icon: {
									path: window.google.maps.SymbolPath.CIRCLE,
									scale: 4,
								},
								offset: '0%',
							},
							{
								icon: {
									path:
										window.google.maps.SymbolPath
											.FORWARD_OPEN_ARROW,
									scale: 4,
								},
								offset: '100%',
							},
						],
					},
				}),
			};
		}

		var GMAPI = this.GMAPI;
		GMAPI.renderer.addListener('directions_changed', this.editTrace);
		if (points !== undefined && points.length > 1) {
			this.createTrace();
		} else {
			GMAPI.map.addListener('click', this.createEndPoints);
			this.setState({
				tip: 'You can set the start point by clicking it on the map.',
			});
		}
	};

	output = data => {
		if (this.props.output) {
			this.props.output(data);
		}
	};

	componentDidMount() {
		window.MapLoader.ready(this.init);
	}

	render() {
		const { length, walk, run, tip } = this.state;
		const {dynamic, showLabels, showFavouriteButton} = this.props;

		return (
			<div className="map">
				{!dynamic && showLabels === false ? (
					''
				) : (
					<div className="row">
						<div className="col-md-8">
							<div className="my-jumbotron">

							<div className="row">
								<div className="col map-tip">
									<span className="map-tip-label">Route length: </span>
									<span>{length}</span>
								</div>
								<div className="col map-tip">
									<span className="map-tip-label">By walk: </span>
									<span>{walk}</span>
								</div>
								<div className="col map-tip">
									<span className="map-tip-label">Running: </span>
									<span>{run}</span>
								</div>
							</div>
							</div>
						</div>
						<div className="col-md-4">
							{this.routeId && showFavouriteButton && <FavouriteButton routeId={this.routeId} />}
						</div>
					</div>
				)}
				<div
					style={{ height: '50vh', width: '100%', marginTop: '5px' }}
					ref={this.mapRef}
				/>
				{dynamic ? <MapTip text={tip} /> : ''}
			</div>
		);
	}
}

Map.defaultProps = {
	showFavouriteButton: true,
};

const mapStateToProps = state => {
	const { id: userId, startingPosition } = state.auth;
	const { lat = 50.085343, lng = 14.435975 } = startingPosition || {};
	return {
		userId,
		userCenter: {
			lat,
			lng
		},

	};
};

const mapDispatchToProps = {
	selectRoute,
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Map);
