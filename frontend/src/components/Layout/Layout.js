import React, { Component } from 'react';
import {connect} from 'react-redux';

import ToolBar from '../Navigation/Navigation';

import './Layout.css';
import FlashMessageBox from "../FlashMessageBox/FlashMessageBox";

class Layout extends Component {
	render() {
		const { flashMessages } = this.props;

		return (
			<div className="layout">
				<ToolBar />
				<div className="container">
					<FlashMessageBox flashMessages={flashMessages} />
					{this.props.children}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { flashMessages } = state.share;

	return {
		flashMessages,
	};
};

export default connect(mapStateToProps)(Layout);
