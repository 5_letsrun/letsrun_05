import React from 'react';

export default ({status, addToFavouriteUsers, removeFromFavouriteUsers}) => {
	return status
		? (
			<span className="ml-3">
				{status === 'remove'
					?
					<button onClick={removeFromFavouriteUsers} className="btn btn-sm btn-danger">
						Remove
					</button>
					:
					<button onClick={addToFavouriteUsers} className="btn btn-sm btn-primary">
						Add
					</button>
				}
			</span>
		)
		: null;
}
