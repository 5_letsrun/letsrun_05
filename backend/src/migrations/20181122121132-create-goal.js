'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface
			.createTable('goals', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				year: {
					allowNull: false,
					type: Sequelize.SMALLINT.UNSIGNED,
				},
				month: {
					allowNull: false,
					type: Sequelize.TINYINT.UNSIGNED,
				},
				amount: {
					allowNull: false,
					type: Sequelize.INTEGER,
				},
				user_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				created_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updated_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			})
			.then(() => {
				queryInterface.addConstraint('goals', ['user_id'], {
					type: 'foreign key',
					references: {
						table: 'users',
						field: 'id',
					},
					onDelete: 'cascade',
				});

				queryInterface.addIndex('goals', ['year', 'month', 'user_id'], {
					unique: true,
				});
			});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('goals');
	},
};
