'use strict';
module.exports = (sequelize, DataTypes) => {
	const PasswordReset = sequelize.define(
		'PasswordReset',
		{
			email: {
				type: DataTypes.STRING(60),
				allowNull: false,
			},
			token: {
				type: DataTypes.STRING(60),
				allowNull: false,
			},
		},
		{
			underscored: true,
			tableName: 'password_resets',
		},
	);
	return PasswordReset;
};
