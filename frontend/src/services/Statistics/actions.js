export const STATISTICS_CHANGE_MONTH = 'STATISTICS_CHANGE_MONTH';
export const STATISTICS_CHANGE_YEAR = 'STATISTICS_CHANGE_YEAR';
export const STATISTICS_SET_CURRENT_DISTANCE =
	'STATISTICS_SET_CURRENT_DISTANCE';
export const STATISTICS_SET_GOAL = 'STATISTICS_SET_GOAL';

export const changeMonth = month => ({
	type: STATISTICS_CHANGE_MONTH,
	payload: { month },
});

export const changeYear = year => ({
	type: STATISTICS_CHANGE_YEAR,
	payload: { year },
});

export const setCurrentDistance = currentDistance => ({
	type: STATISTICS_SET_CURRENT_DISTANCE,
	payload: { currentDistance },
});

export const setGoal = goal => ({
	type: STATISTICS_SET_GOAL,
	payload: { goal },
});
