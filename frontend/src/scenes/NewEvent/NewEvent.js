import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import DatePicker from '../../components/ReactDatePicker/ReactDatePicker';

import FontIcon from '../../components/FontIcon/FontIcon';
import Axios from '../../axios';
import Map from '../../components/Map/Map';
import {
	IS_EMPTY,
	MIN_DATE,
	MIN_TIME,
	IS_DATE,
	IS_TIME,
	Validator
} from "../../utils/Validator/Validator";
import FlashMessage, {FLASH_DANGER, FLASH_SUCCESS} from "../../components/FlashMessageBox/FlashMessage/FlashMessage";
import {createFlashMessage} from "../../services/Share/shareActions";
import PageTitle from '../../components/PageTitle/PageTitle';

import './NewEvent.css';


class NewEvent extends Component {
	state = {
		form: {
			name: '',
			date: null,
			time: null,
			description: '',
			submitted: false,
		},
		route: null,
		routes: [],
		trace: '',
		distance: '',
		start: '',
		currentRoute: '',
		reloadPage: this.props.reload,
		errorMessage: '',
	};

	validator = new Validator([
		{
			field: 'name',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'Field \'Event name\' cannot be empty.',
				},
			],
		},
		{
			field: 'date',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'Field \'Date\' cannot be empty.',
				},
				{
					method: IS_DATE,
					validWhen: true,
					errorMsg: 'Invalid date format.',
				},
				{
					method: MIN_DATE,
					arg: new Date(),
					validWhen: true,
					errorMsg: 'Date cannot be earlier than today.',
				},
			],
		},
		{
			field: 'time',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'Field \'Time\' cannot be empty.',
				},
				{
					method: IS_TIME,
					validWhen: true,
					errorMsg: 'Invalid time format.',
				},
				{
					method: MIN_TIME,
					arg: {
						field: 'date',
						minTime: new Date(),
					},
					validWhen: true,
					errorMsg: 'Time cannot be earlier than now.',
				},
			],
		},
	]);

	onSubmitHandler = event => {
		event.preventDefault();
		const form = {
			...this.state.form,
			submitted: true,
		};
		this.setState({form});

		const {trace, distance, start} = this.state;
		if (!trace || !distance || !start) {
			this.setState({
				errorMessage: 'No route was selected.',
			});
		} else {
			this.setState({
				errorMessage: '',
			})
		}

		if (this.validator.validate(form)) {
			const {name, date, time, description} = form;
			const term = moment(`${moment(date).format('YYYY-MM-DD')} ${moment(time).format('HH:mm')}`, 'YYYY-MM-DD HH:mm');

			Axios.post('/events/create', {
				name,
				term,
				description,
				user_id: this.props.userId,
				trace,
				distance,
				start,
			})
				.then(response => {
					const {name} = response.data.event;
					this.props.addFlashMessage({
						type: FLASH_SUCCESS,
						message: `Event '${name}' has been successfully created.`,
					});
					this.props.history.push('/');
				});
		}
	};

	onChangeHandler = event => {
		const fieldName = event.target.name;
		const value = event.target.value;

		const form = {
			...this.state.form,
			[fieldName]: value,
		};

		this.setState({form});
	};

	onDateChangeHandler = date => {
		const form = {
			...this.state.form,
			date,
		};

		this.setState({form});
	};

	onTimeChangeHandler = time => {
		const form = {
			...this.state.form,
			time,
		};

		this.setState({form});
	};

	handleRouteSelect = routeData => {
		const {distance, trace, start} = routeData;
		this.setState({
			distance,
			trace,
			start,
		});
	};

	onRouteSelectHandler = (e) => {
		const route = this.state.routes.find(route => route.route_id === parseInt(e.target.value, 10));
		this.setState({
			route,
		})
	};

	componentDidMount() {
		Axios.get(`/routes/favorite/${this.props.userId}`).then(response => {
			this.setState({routes: response.data.favouriteRoutes});
		});
	}

	render() {
		const {routes, route, errorMessage} = this.state;

		return (
			<div id="new-event">
				<PageTitle>New Event</PageTitle>
				<Map
					id="NewEventMap"
					dynamic={true}
					output={this.handleRouteSelect}
					routeId={route && route.route_id}
					key={route && route.route_id}
					showFavouriteButton={false}
				/>
				<div className="my-jumbotron">
					<FlashMessage type={FLASH_DANGER}>
						{errorMessage}
					</FlashMessage>
					<button
						type="button"
						className="btn btn-info dropdown-toggle"
						data-toggle="dropdown"
						aria-haspopup="true"
						aria-expanded="false"
						id="routes-select"
					>
						{route ? (
							route.name
						) : (
							"Select favourite route"
						)}
					</button>
					<div className="dropdown-menu" onClick={this.onRouteSelectHandler}>
						{routes ? (
							routes.map(favouriteRoute => {
									return (
										<option key={favouriteRoute.route_id} value={favouriteRoute.route_id}>
											{favouriteRoute.name ? favouriteRoute.name : favouriteRoute.route_id}
										</option>
									);
								}
							)
						) : (
							<div>No favourite routes</div>
						)}
					</div>
					<form onSubmit={this.handleFormSubmit}>
						<div className="row">
							<div className="form-group col-md-4">
								<label htmlFor="name">Event name</label>
								<input
									type="text"
									name="name"
									className={this.validator.getClassNames(
										'name',
									)}
									onChange={this.onChangeHandler}
								/>
								<span className="help-block">
									{this.validator.getErrorMessage(
										'name',
									)}
								</span>
							</div>
						</div>
						<div className="form-row">
							<div className="form-group col-md-3">
								<label htmlFor="date"><FontIcon icon='calendar-alt'/> Date</label>
								<DatePicker
									className={this.validator.getClassNames(
										'date',
									)}
									dropdownMode="select"
									onChange={this.onDateChangeHandler}
									selected={this.state.form.date}
									minDate={new Date()}
								/>
								<span className="help-block">
									{this.validator.getErrorMessage(
										'date',
									)}
								</span>
							</div>
							<div className="form-group col-md-2">
								<label htmlFor="time"><FontIcon icon='clock'/> Time</label>
								<DatePicker
									className={this.validator.getClassNames(
										'date',
									)}
									dropdownMode="select"
									selected={this.state.form.time}
									onChange={this.onTimeChangeHandler}
									showTimeSelect
									showTimeSelectOnly
									timeIntervals={15}
									timeFormat="HH:mm"
									dateFormat="HH:mm"
								/>
								<span className="help-block">
									{this.validator.getErrorMessage(
										'time',
									)}
								</span>
							</div>
						</div>
						<div className="form-group">
							<label htmlFor="description">
								Description
							</label>
							<textarea
								className="form-control"
								name="description"
								rows="3"
								onChange={this.onChangeHandler}
							/>
						</div>

						<button
							type="submit"
							className="btn btn-info btn-block"
							onClick={this.onSubmitHandler}
						>
							Create Event
						</button>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		userId: state.auth.id,
		trace: state.route.trace,
		distance: state.route.distance,
		start: state.route.start,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		addFlashMessage: payload => dispatch(createFlashMessage(payload)),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(NewEvent);
