import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';

import { signUp } from '../../services/Auth/authActions';
import { createFlashMessage } from '../../services/Share/shareActions';
import PositionPicker from "../../components/PositionPicker/PositionPicker";
import Modal from '../../components/Modal/Modal';
import FontIcon from '../../components/FontIcon/FontIcon';
import FlashMessageBox from '../../components/FlashMessageBox/FlashMessageBox';
import {
	Validator,
	IS_EMPTY,
	IS_EMAIL,
	MIN_LENGTH,
	IS_EQUAL_TO_FIELD,
} from '../../utils/Validator/Validator';

import '../Login/Login.css';


class SignUp extends Component {
	state = {
		form: {
			firstName: '',
			lastName: '',
			email: '',
			password: '',
			confirmPassword: '',
			submitted: false,
			lat: '',
			lng: '',
		},
		errorMessage: '',
	};

	validator = new Validator([
		{
			field: 'firstName',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'Field "First name" cannot be empty.',
				},
			],
		},
		{
			field: 'lastName',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'Field "Last name" cannot be empty.',
				},
			],
		},
		{
			field: 'email',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'Field "E-mail" cannot be empty.',
				},
				{
					method: IS_EMAIL,
					validWhen: true,
					errorMsg: 'Please, provide the correct email.(example: john123@gmail.com)',
				},
			],
		},
		{
			field: 'password',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'Field "Password" cannot be empty.',
				},
				{
					method: MIN_LENGTH,
					arg: 6,
					validWhen: true,
					errorMsg: 'Password must be at least 6 characters long.',
				},
			],
		},
		{
			field: 'confirmPassword',
			rules: [
				{
					method: IS_EMPTY,
					validWhen: false,
					errorMsg: 'Field "Confirm password" cannot be empty.',
				},
				{
					method: IS_EQUAL_TO_FIELD,
					arg: 'password',
					validWhen: true,
					errorMsg: 'Passwords does not match',
				},
			],
		},
	]);

	onSubmitHandler = event => {
		event.preventDefault();

		const form = {
			...this.state.form,
			submitted: true,
		};
		this.setState({ form });
		if (this.validator.validate(form)) {
			const { email, firstName, lastName, password, lat, lng } = form;
			this.props.onSubmit({
				email,
				firstName,
				lastName,
				password,
				lat,
				lng,
			});
		}
	};

	onChangeHandler = event => {
		const fieldName = event.target.name;
		const value = event.target.value;
		const form = {
			...this.state.form,
			[fieldName]: value,
		};
		this.setState({ form });
	};

	onMapClickHandler = ({lat, lng}) => {
		const form = {
			...this.state.form,
			lat,
			lng,
		};
		this.setState({form});
	};

	render() {
		const {
			firstName,
			lastName,
			email,
			password,
			confirmPassword,
			lat,
			lng
		} = this.state.form;

		const { signUpSuccess, flashMessages } = this.props;

		return signUpSuccess ? (
			<Redirect to="/login" />
		) : (
			<div className="login">
				<div className="login-container">
					<Link to="/login" className="back-link btn btn-outline-info"><FontIcon icon="arrow-left" /> Back</Link>
					<FlashMessageBox flashMessages={flashMessages} />
					<form onSubmit={this.onSubmitHandler}>
						<div className="form-row form-group">
							<div className="col">
								<label>First name</label>
								<input
									type="text"
									className={this.validator.getClassNames(
										'firstName',
									)}
									value={firstName}
									name="firstName"
									onChange={this.onChangeHandler}
								/>
								<span className="help-block">
									{this.validator.getErrorMessage(
										'firstName',
									)}
								</span>
							</div>
							<div className="col">
								<label>Last name</label>
								<input
									type="text"
									className={this.validator.getClassNames(
										'lastName',
									)}
									value={lastName}
									name="lastName"
									onChange={this.onChangeHandler}
								/>
								<span className="help-block">
									{this.validator.getErrorMessage('lastName')}
								</span>
							</div>
						</div>
						<div className="form-group">
							<label>E-Mail</label>
							<input
								type="text"
								className={this.validator.getClassNames(
									'email',
								)}
								value={email}
								name="email"
								onChange={this.onChangeHandler}
							/>
							<span className="help-block">
								{this.validator.getErrorMessage('email')}
							</span>
						</div>
						<div className="form-group">
							<label>Password</label>
							<input
								type="password"
								className={this.validator.getClassNames(
									'password',
								)}
								name="password"
								value={password}
								onChange={this.onChangeHandler}
							/>
							<span className="help-block">
								{this.validator.getErrorMessage('password')}
							</span>
						</div>
						<div className="form-group">
							<label>Confirm password</label>
							<input
								type="password"
								className={this.validator.getClassNames(
									'confirmPassword',
								)}
								value={confirmPassword}
								name="confirmPassword"
								onChange={this.onChangeHandler}
							/>
							<span className="help-block">
								{this.validator.getErrorMessage(
									'confirmPassword',
								)}
							</span>
						</div>
						<div className="form-row form-group">
							<div className="col">
								<Modal
									btnTrigger={<FontIcon icon="map-marker-alt" />}
									btnSubmit="Choose this coordinate"
									title="Find your starting position!"
								>
									<PositionPicker onClick={this.onMapClickHandler}/>
								</Modal>
							</div>
							<div className="col">
								<label>Lat</label>
								<input
									type="text"
									className="form-control"
									value={lat}
									name="lat"
									readOnly
									disabled
								/>
							</div>
							<div className="col">
								<label>Lng</label>
								<input
									type="text"
									className="form-control"
									value={lng}
									name="lng"
									readOnly
									disabled
								/>
							</div>
						</div>
						<button
							type="submit"
							className="btn btn-info btn-block"
						>
							Sign Up
						</button>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { signUpSuccess } = state.auth;
	const { flashMessages } = state.share;

	return {
		signUpSuccess,
		flashMessages,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onSubmit: payload => dispatch(signUp(payload)),
		addFlashMessage: payload => dispatch(createFlashMessage(payload)),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(SignUp);
