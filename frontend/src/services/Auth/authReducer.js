import {
	AUTH_SUCCESS,
	AUTH_START,
	LOGOUT,
	SIGNUP_SUCCESS,
	UPDATE_USER,
} from './authActions';

const initialState = {
	token: null,
	id: null,
	email: null,
	firstName: null,
	lastName: null,
	startingPosition: null,
	signUpSuccess: null,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case AUTH_START: {
			return {
				...state,
				error: null,
			};
		}
		case AUTH_SUCCESS: {
			const {token, id, email, firstName, lastName, startingPosition} = action.payload;
			return {
				...state,
				token,
				id,
				email,
				firstName,
				lastName,
				startingPosition,
			};
		}
		case LOGOUT: {
			return {
				...state,
				error: null,
				token: null,
				id: null,
				email: null,
				firstName: null,
				lastName: null,
				startingPosition: null,
			};
		}
		case SIGNUP_SUCCESS: {
			const {signUpSuccess} = action.payload;
			return {
				...state,
				signUpSuccess,
			};
		}
		case UPDATE_USER: {
			const {id, email, firstName, lastName, startingPosition} = action.payload;
			return {
				...state,
				id,
				email,
				firstName,
				lastName,
				startingPosition,
			};
		}
		default:
			return state;
	}
};
