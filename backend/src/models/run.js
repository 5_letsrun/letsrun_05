'use strict';
module.exports = (sequelize, DataTypes) => {
	const Run = sequelize.define(
		'Run',
		{
			time: {
				type: DataTypes.TIME,
				allowNull: false,
			},
			eventId: {
				type: DataTypes.INTEGER.UNSIGNED,
				allowNull: false,
				field: 'event_id',
			},
			userId: {
				type: DataTypes.INTEGER.UNSIGNED,
				allowNull: false,
				field: 'user_id',
			},
			created_at: {
				type: DataTypes.DATE,
			},
		},
		{
			underscored: true,
			tableName: 'runs',
		},
	);
	Run.associate = function(models) {
		Run.belongsTo(models.Event);

		Run.belongsTo(models.User);
	};
	return Run;
};
