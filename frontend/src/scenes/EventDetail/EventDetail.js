import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import CommentBox from '../../components/CommentBox/CommentBox';
import axios from '../../axios';
import Map from '../../components/Map/Map';
import Recorder from '../../components/Recorder/Recorder';
import FavouriteUser from '../../components/FavouriteUser/FavouriteUser';
import PageTitle from '../../components/PageTitle/PageTitle';
import FlashMessage from '../../components/FlashMessageBox/FlashMessage/FlashMessage';
import FontIcon from "../../components/FontIcon/FontIcon";

import './EventDetail.css';


class EventDetail extends Component {
	state = {
		startDateTime: null,
		eventName: '',
		host: '',
		datetime: '',
		distance: '',
		estimatedTime: '',
		details: '',
		participants: [],
		comments: null,
		routeId: '',
		showMap: false,
		favouriteUsers: [],
	};

	componentDidMount() {
		this.eventId = this.props.match.params.id;

        axios.get(`/user/${this.props.userId}/favourite`).then(res => {
            const {favouriteUsers} = res.data;

			this.setState({
				favouriteUsers: favouriteUsers.map(({friend_id}) => friend_id),
			});
        });

		axios
			.get('/events/' + this.eventId, {
				userId: this.props.userId,
				eventId: this.eventId,
			})
			.then(res => {
				const event = res.data.data;
				const term = new Date(event.term);

				this.setState({
					startDateTime: term,
					eventName: event.name,
					host: event.User.firstName + ' ' + event.User.lastName,
					datetime: moment(event.term).format('DD.MM.YYYY, HH:mm'),
					distance: event.Route.distance,
					details: event.description,
					participants: event.Participants.map(participant => {
						return {
							id: participant.id,
							name:
								participant.firstName +
								' ' +
								participant.lastName,
						};
					}),
					comments: event.Comments.map(comment => {
						return {
							id: comment.id,
							name: `${comment.User.firstName} ${comment.User.lastName}`,
							email: comment.User.email,
							datetime: moment(comment.created_at).format('DD.MM.YYYY, HH:mm'),
							content: comment.content,
						};
					}),
					routeId: event.Route.id,
					showMap: true,
				});
			});

		axios
			.get('/events/isEventCompleteByUser', {
				params: {
					userId: this.props.userId,
					eventId: this.eventId,
				},
			})
			.then(res => {
				let isEventComplete = res.data.isEventComplete;

				this.setState({
					isEventComplete: isEventComplete,
				});
			});

		axios
			.get(`/user/isUserRegisteredForEvent`, {
				params: {
					userId: this.props.userId,
					eventId: this.eventId,
				},
			})
			.then(res => {
				let isUserRegisteredForEvent =
					res.data.isUserRegisteredForEvent;

				this.setState({
					isUserRegisteredForEvent: isUserRegisteredForEvent,
				});
			});
	}

	getFavouriteUserStatus = participantId => {
		const {userId} = this.props;
		const {favouriteUsers} = this.state;

		return userId !== participantId
			? (
				favouriteUsers.includes(participantId)
					? 'remove'
					: 'add'
			)
			: null;
	};

	addToFavouriteUsers = participantId => {
		const favouriteUsers = [...this.state.favouriteUsers];

		axios
			.post(`/user/${this.props.userId}/favourite/${participantId}`)
			.then(() => {
				favouriteUsers.push(participantId);
				this.setState({favouriteUsers});
			});
	};

	removeFromFavouriteUsers = participantId => {
		let favouriteUsers = [...this.state.favouriteUsers];
		const {userId} = this.props;

		axios
			.delete(`/user/${userId}/favourite/${participantId}`)
			.then(() => {
				favouriteUsers = favouriteUsers.filter(friendId => friendId !== participantId);

				this.setState({favouriteUsers});
			});
	};

	onAddingNewCommentHandler = newComment => {
		const {userId, firstName, lastName, email} = this.props;

		axios
			.post('/events/addComment', {
				content: newComment,
				eventId: this.eventId,
				userId,
			})
			.then(res => {
				const comments = [...this.state.comments];
				const {id, content, created_at} = res.data.comment;

				comments.unshift({
					id,
					name: `${firstName} ${lastName}`,
					email,
					datetime: moment(created_at).format('DD.MM.YYYY, HH:mm'),
					content,
				});

				this.setState({comments});
			});
	};

	checkStartTime = () => {
		const fromTime = moment().subtract({ hours: 6 });
		const toTime = moment().add({ hours: 6 });
		const now = moment(this.state.startDateTime);

		return now.isSameOrBefore(toTime) && now.isSameOrAfter(fromTime);
	};

	handleSubmit = () => {
		const participants = [...this.state.participants];

		axios
			.post('/events/participants', {
				userId: this.props.userId,
				id: this.eventId,
			})
			.then(() => {
				participants.unshift({
					name:
						localStorage.getItem('firstName') +
						' ' +
						localStorage.getItem('lastName'),
				});

				this.setState({participants});
			});

		this.setState({
			isUserRegisteredForEvent: true,
		});
	};

	finishRunHandler = () => {
		var user = this.props.userId;
		var event = this.eventId;
		var time = this.props.time;
		axios.post('/runs/create', {
			time: time,
			userId: user,
			eventId: event,
		});
	};

	render() {
		const {
			startDateTime,
			eventName,
			host,
			datetime,
			distance,
			details,
			participants,
			comments,
			routeId,
			showMap,
		} = this.state;


		let finishButton;
		let registerButton;
		let stopWatch;
		let statisticsPath = '/statistics';
		let checkTime = this.checkStartTime();

		if (
			this.state.isUserRegisteredForEvent === true &&
			this.state.isEventComplete === false &&
			checkTime === true &&
			this.props.time !== '' &&
			this.props.time !== '00:00:00'
		) {
			finishButton = (
				<a href={statisticsPath}>
					<button
						style={{width: '170px'}}
						type="button"
						className="text-uppercase btn btn-primary btn-lg"
						onClick={this.finishRunHandler}
					>
						Finish run!
					</button>
				</a>
			);
			stopWatch = <Recorder/>;
		} else if (
			this.state.isUserRegisteredForEvent === true &&
			this.state.isEventComplete === false &&
			checkTime === true
		) {

			stopWatch = <Recorder/>;
		} else if (
			this.state.isEventComplete === false &&
			this.state.isUserRegisteredForEvent === false
		) {
			registerButton = (
				<button
					type="button"
					className="text-uppercase btn btn-info btn-lg"
					onClick={this.handleSubmit}
				>
					Register for an event!
				</button>
			);
		}

		let warningText;
		if (!checkTime) {
			if (startDateTime > (new Date())) {
				warningText = <FlashMessage type="info"><FontIcon icon="info-circle"/> You can start to run 6 hours before the beginning of the event.</FlashMessage>
			} else {
				warningText = <FlashMessage type="warning"><FontIcon icon="exclamation-circle"/> Unfortunately, this event is over. Try another one!</FlashMessage>
			}
		}

		return (
			<div className="container" id="event-container">
				<PageTitle>Event '{eventName}'</PageTitle>
					<div className="my-jumbotron text-center">
						{registerButton}
						{warningText}
						{stopWatch}
						<div style={{marginTop: '5px'}}>{finishButton}</div>
					</div>
				{showMap ? (
					<Map
						id="EventDetailMap"
						routeId={routeId}
						dynamic={false}
						zoom={16}
						showFavorite={true}
					/>
				) : (
					''
				)}

				<div id="detail" className="my-jumbotron">
					<div className="row">
						<div className="col-md-6">
							<div className="event-item">
								<p className="event-item-label">Host:</p>
								<p>{host}</p>
							</div>
							<div className="event-item">
								<p className="event-item-label">Date and time:</p>
								<p>{datetime}</p>
							</div>
							<div className="event-item">
								<p className="event-item-label">Distance:</p>
								<p>{distance} km</p>
							</div>
							<div className="event-item">
								<p className="event-item-label">Detail:</p>
								<p style={{textAlign: 'justify'}}>{details}</p>
							</div>
						</div>

						<div className="col-md-6">
							<p className="event-item-label">Participants</p>
							{participants.map((participant, index) => {
                                let statisticsPath = '/statistics/' + participant.id;

								return (
									<p key={participant.id}>
										<a href={statisticsPath}>{participant.name}</a>
											<FavouriteUser
												key={index}
												status={this.getFavouriteUserStatus(participant.id)}
												addToFavouriteUsers={() => this.addToFavouriteUsers(participant.id)}
												removeFromFavouriteUsers={() => this.removeFromFavouriteUsers(participant.id)}
											/>
									</p>
								);
							})}
						</div>
					</div>
				</div>
				<div className="my-jumbotron">
					<CommentBox
						addedNewComment={this.onAddingNewCommentHandler}
						comments={comments}
					/>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	const {id, firstName, lastName, email} = state.auth;
	const userId = parseInt(id, 10);
	return {
		userId,
		firstName,
		lastName,
		email,
		time: state.recorder.time,
	};
};

export default connect(
	mapStateToProps,
	null,
)(EventDetail);
