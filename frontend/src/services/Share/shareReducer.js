import {
	ADD_FLASH_MESSAGE,
	REMOVE_ALL_FLASH_MESSAGES,
	REMOVE_FLASH_MESSAGE,
} from './shareActions';

const initialState = {
	flashMessages: [],
};

export default (state = initialState, action) => {
	let flashMessages = [];

	switch (action.type) {
		case ADD_FLASH_MESSAGE:
			const { id, type, message } = action.payload;
			flashMessages = [ ...state.flashMessages ];
			flashMessages.push({
				id,
				type,
				message,
			});
			return {
				...state,
				flashMessages,
			};
		case REMOVE_FLASH_MESSAGE:
			const { flashId } = action.payload;
			flashMessages = state.flashMessages.filter(flash => {
				return flash.id !== flashId;
			});
			return {
				...state,
				flashMessages,
			};
		case REMOVE_ALL_FLASH_MESSAGES:
			flashMessages = {};
			return {
				...state,
				flashMessages,
			};
		default:
			return state;
	}
};
