import React, { Component } from 'react';
import { YearPicker } from 'react-dropdown-date';
import { connect } from 'react-redux';
import { changeYear } from '../../services/Statistics/actions';

class YearDropdown extends Component {
	constructor(props) {
		super(props);
		const date = new Date();
		const year = date.getFullYear();

		this.props = { year: year, month: null, day: null };
	}

	render() {
		const { changeYear } = this.props;

		return (
			<span id="YearDropdown">
				<label>Rok</label>
				<YearPicker
					defaultValue={this.props.year}
					start={2018}
					end={2030}
					value={this.props.year}
					required={true}
					onChange={changeYear}
					id={'YearPicker'}
				/>
			</span>
		);
	}
}

const mapStateToProps = state => {
	return {
		year: state.year,
	};
};

const mapDispatchToProps = {
	changeYear,
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(YearDropdown);
