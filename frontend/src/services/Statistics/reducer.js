import {
	STATISTICS_CHANGE_YEAR,
	STATISTICS_CHANGE_MONTH,
	STATISTICS_SET_CURRENT_DISTANCE,
	STATISTICS_SET_GOAL,
} from './actions';

const initialState = {
	year: '',
	month: '',
	currentDistance: '',
};

export const statisticsReducer = (state = initialState, action) => {
	switch (action.type) {
		case STATISTICS_CHANGE_YEAR: {
			const { year } = action.payload;
			return { ...state, year: year };
		}

		case STATISTICS_CHANGE_MONTH: {
			const { month } = action.payload;
			return { ...state, month: month };
		}

		case STATISTICS_SET_CURRENT_DISTANCE: {
			const { currentDistance } = action.payload;
			return { ...state, currentDistance: currentDistance };
		}

		case STATISTICS_SET_GOAL: {
			const { goal } = action.payload;
			return { ...state, goal: goal };
		}

		default:
			return state;
	}
};
