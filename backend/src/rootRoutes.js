import { Router } from 'express';

import statisticsRoutes from './modules/statistics/routes';
import authRoutes from './modules/auth/authRoutes';
import routesRoutes from './modules/routes/routesRoutes';
import userRoutes from './modules/user/userRoutes';
import eventRoutes from './modules/events/eventRoutes';
import runsRoutes from './modules/runs/runRoutes';

import { verifyToken } from './modules/auth/tokener';

const router = Router();

router.use('/api/auth', authRoutes);
router.use('/api/Statistics', verifyToken, statisticsRoutes);
router.use('/api/routes', verifyToken, routesRoutes);
router.use('/api/user', verifyToken, userRoutes);
router.use('/api/events', verifyToken, eventRoutes);
router.use('/api/runs', verifyToken, runsRoutes);

export default router;
