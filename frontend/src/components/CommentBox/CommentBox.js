import React, { Component } from 'react';
import Comment from './Comment/Comment';

import './CommentBox.css';

export default class extends Component {
	state = {
		comments: null,
		newComment: '',
	};

	constructor(props) {
		super(props);
		this.inputRef = React.createRef();
	}

	onTypingHandler = e => {
		this.setState({ newComment: e.target.value });
	};

	onAddHandler = () => {
		this.props.addedNewComment(this.state.newComment);
		this.setState({ newComment: '' });
		this.inputRef.current.focus();
	};

	render() {
		const comments = this.props.comments
			? this.props.comments.map(comment => {
					return <Comment key={comment.id} {...comment} />;
			  })
			: null;

		return (
			<div className="comment-box">
				<p className="text-center font-weight-bold">Comments</p>
				<div className="input-group mb-3">
					<input
						ref={this.inputRef}
						onKeyPress={e => {
							if (e.key === 'Enter') {
								this.onAddHandler();
							}
						}}
						onChange={this.onTypingHandler}
						value={this.state.newComment}
						type="text"
						className="form-control"
						placeholder="Type a comment..."
					/>
					<div className="input-group-append">
						<button
							onClick={this.onAddHandler}
							className="btn btn-info"
							type="button"
						>
							Add
						</button>
					</div>
				</div>
				<div className="comments border border-light rounded">
					<ul className="list-group list-group-flush">{comments}</ul>
				</div>
			</div>
		);
	}
}
