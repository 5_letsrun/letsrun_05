import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import { statisticsReducer } from '../services/Statistics/reducer';
import authReducer from '../services/Auth/authReducer';
import shareReducer from '../services/Share/shareReducer';
import { routesReducer } from '../services/Routes/reducer';
import { recorderReducer } from '../services/Recorder/reducer';

const persistConfig = {
	key: 'root',
	storage,
	whitelist: [],
};

export const createRootReducer = () => {
	const rootReducer = combineReducers({
		statistics: statisticsReducer,
		auth: authReducer,
		share: shareReducer,
		route: routesReducer,
		recorder: recorderReducer,
	});

	return persistReducer(persistConfig, rootReducer);
};
