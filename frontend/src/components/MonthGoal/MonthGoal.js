import React, { Component } from 'react';
import Axios from '../../axios';
import './MonthGoal.css';
import { connect } from 'react-redux';
import { setGoal } from '../../services/Statistics/actions';

class MonthGoal extends Component {
	constructor(props) {
		super(props);

		const date = new Date();
		const currentMonth = date.getMonth() + 1;
		const currentYear = date.getFullYear();

		this.state = {
			goal: null,
			currentDistance: null,
			currentProgress: null,
			user: this.props.user,
			month: currentMonth,
			year: currentYear,
		};
	}

	handleSubmit = () => {
		var goal = document.getElementById('goalInput').value;
		var user = this.state.user;
		var month = this.props.month;
		var year = this.props.year;
		this.setState({ goal: goal, year: year, month: month, user: user });
		let currentDistance;
		let currentProgress;
		Axios.post(`/statistics/goal`, {
			goal: goal,
			year: year,
			month: month,
			user: user,
		});

		currentDistance = this.state.currentDistance;
		if (goal !== 0 && currentDistance !== 0) {
			currentProgress = (currentDistance / goal) * 100;
		} else if (goal === 0 && currentDistance !== 0) {
			currentProgress = 100;
		} else currentProgress = 0;

		let currentDistanceFixed = parseFloat(
			Math.round(currentDistance * 100) / 100,
		).toFixed(2);
		this.setState({
			currentProgress: currentProgress,
			currentDistance: currentDistanceFixed,
			goal: goal,
		});
	};

	componentDidUpdate(prevProps) {
		let currentDistance;
		let currentProgress;
		let month = this.props.month;
		let year = this.props.year;
		let goal = this.props.goal;
		if (prevProps.goal !== this.props.goal) {
			Axios.get(`/statistics/goal`, {
				params: {
					month: month,
					year: year,
					user: this.props.statisticsUser,
				},
			}).then(response => {
				goal = response.data.goal;

				this.setState({
					goal: goal,
				});
				this.props.setGoal(goal);
			});

			Axios.get(`/statistics/currentDistance`, {
				params: {
					month: month,
					year: year,
					user: this.props.statisticsUser,
				},
			}).then(response => {
				currentDistance = response.data.currentDistance;
				if (goal !== 0 && currentDistance !== 0) {
					currentProgress = (currentDistance / goal) * 100;
				} else if (goal === 0 && currentDistance !== 0) {
					currentProgress = 100;
				} else currentProgress = 0;

				let currentDistanceFixed = parseFloat(
					Math.round(currentDistance * 100) / 100,
				).toFixed(2);
				this.setState({
					currentProgress: currentProgress,
					currentDistance: currentDistanceFixed,
					goal: goal,
				});
				this.props.setGoal(goal);
			});
		}
	}

	componentDidMount() {
		let currentDistance;
		let currentProgress;
		let month = this.state.month;
		let year = this.state.year;
		let goal;

		Axios.get(`/statistics/goal`, {
			params: {
				month: month,
				year: year,
				user: this.props.statisticsUser,
			},
		}).then(response => {
			goal = response.data.goal;

			this.setState({
				goal: goal,
			});
		});
		this.props.setGoal(goal);

		Axios.get(`/statistics/currentDistance`, {
			params: {
				month: month,
				year: year,
				user: this.props.statisticsUser,
			},
		}).then(response => {
			currentDistance = response.data.currentDistance;

			if (goal !== 0 && currentDistance !== 0) {
				currentProgress = (currentDistance / goal) * 100;
			} else if (goal === 0 && currentDistance !== 0) {
				currentProgress = 100;
			} else currentProgress = 0;

			let currentDistanceFixed = parseFloat(
				Math.round(currentDistance * 100) / 100,
			).toFixed(2);

			this.setState({
				currentDistance: currentDistanceFixed,
				currentProgress: currentProgress,
			});
		});
	}

	render() {
		let goalSetter;
		let goalButton;
		if (
			parseInt(this.props.user, 10) ===
			parseInt(this.props.statisticsUser, 10)
		) {
			goalSetter = (
				<div id="goalInputForm" className="input-group col-md-12	">
					<label className="date-label  goal-label-setter 	 col-md-7">
						Set goal:
					</label>
					<input
						type="text"
						className="form-control goalInput"
						id="goalInput"
						defaultValue={this.state.goal}
						value={this.state.goalInput}
						onChange={event =>
							this.setState({
								goalInput: event.target.value.replace(/\D/, ''),
							})
						}
					/>
					<div className="input-group-append">
						<span className="input-group-text" id="basic-addon2">
							km
						</span>
					</div>
				</div>
			);
			goalButton = (
				<button
					id="goalButton"
					className="btn btn-info col-md-1"
					onClick={this.handleSubmit}
				>
					OK
				</button>
			);
		}
		return (
			<section className="container">
				<div className="row goalRow">
					<div className=" col-sm-6">
						<label className="date-label col-md-4 goal-label">
							Current goal:
						</label>
						<label
							id="monthGoal"
							className="date-label col-md-4 goal-label"
						>
							{this.state.goal} km
						</label>
					</div>
				</div>
				<div className="row goalInputRow col-sm-12">
					{goalSetter} {goalButton}
				</div>
				<div className="form-horizontal row progress-bar-container">
					<div className="progress  col-sm-12">
						<div
							id="progress"
							className="progress-bar progress-bar-striped bg-success .progress-bar-animated"
							style={{ width: this.state.currentProgress + '%' }}
							role="progressbar"
							aria-valuenow="25"
							aria-valuemin="0"
							aria-valuemax="100"
						>
							{this.state.currentDistance} / {this.state.goal}
						</div>
					</div>
				</div>
			</section>
		);
	}
}

const mapStateToProps = state => {
	return {
		currentDistance: state.statistics.currentDistance,
		month: state.statistics.month,
		year: state.statistics.year,
		goal: state.statistics.goal,
		user: state.auth.id,
	};
};

const mapDispatchToProps = {
	setGoal,
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(MonthGoal);
