import db from '../../models';

const Run = db.Run;

export const createRun = async (req, res) => {
	var time = req.body.time;
	var eventId = req.body.eventId;
	var userId = req.body.userId;

	await Run.create({
		time: time,
		eventId: eventId,
		userId: userId,
	});

	return res.status(200);
};

export const getRunByUserIdAndEventId = async (userId, eventId) => {
	return await Run.findOne({
		where: {
			userId,
			eventId,
		},
	});
};
