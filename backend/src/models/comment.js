'use strict';
module.exports = (sequelize, DataTypes) => {
	const Comment = sequelize.define(
		'Comment',
		{
			content: {
				type: DataTypes.TEXT,
				allowNull: false,
			},
			eventId: {
				type: DataTypes.INTEGER.UNSIGNED,
				allowNull: false,
				field: 'event_id',
			},
			userId: {
				type: DataTypes.INTEGER.UNSIGNED,
				allowNull: false,
				field: 'user_id',
			},
		},
		{
			underscored: true,
			tableName: 'comments',
		},
	);
	Comment.associate = function(models) {
		Comment.belongsTo(models.Event);

		Comment.belongsTo(models.User);
	};
	return Comment;
};
