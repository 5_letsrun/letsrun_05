import React, { Component } from 'react';
import { connect } from 'react-redux';

import PageTitle from '../../components/PageTitle/PageTitle';
import axios from '../../axios';
import FavouriteUser from '../../components/FavouriteUser/FavouriteUser';

class FavouriteUsers extends Component {
	state = {
		favouriteUsers: [],
		favouriteUsersId: [],
	};

	addToFavouriteUsers = participantId => {
		const favouriteUsers = [...this.state.favouriteUsersId];

		axios
			.post(`/user/${this.props.userId}/favourite/${participantId}`)
			.then(() => {
				favouriteUsers.push(participantId);
				this.setState({ favouriteUsers });
			});
	};

	removeFromFavouriteUsers = participantId => {
		let favouriteUsers = [...this.state.favouriteUsers];
		const { userId } = this.props;
		let index;

		for (var i = 0; i < favouriteUsers.length; i++) {
			if (favouriteUsers[i]['id'] === participantId) {
				index = i;
			}
		}

		axios.delete(`/user/${userId}/favourite/${participantId}`).then(() => {
			favouriteUsers.splice(index, 1);

			this.setState({ favouriteUsers });
		});
	};

	getFavouriteUserStatus = participantId => {
		const { userId } = this.props;
		const { favouriteUsersId } = this.state;

		return userId !== participantId
			? favouriteUsersId.includes(participantId)
				? 'remove'
				: 'add'
			: null;
	};

	async componentDidMount() {
		let favouriteUsersId;
		let users = [];

		await axios.get(`/user/${this.props.userId}/favourite`).then(res => {
			const { users } = res.data;
			//favouriteUsersId = users.map(({ id }) => id, firstName, lastName);

			this.setState({
				favouriteUsersId: users.map(({ id }) => id),
				favouriteUsers: users,
			});
		});
	}

	render() {
		const favouriteUsers = this.state.favouriteUsers;

		return (
			<div id="event-container" className="event-container">
				<PageTitle>Favourite Users</PageTitle>

				<ul className="date-container my-jumbotron mb-2">
					<div id="favouriteUsers">
						{favouriteUsers.map((favouriteUser, index) => {
							let userPath = '/statistics/' + favouriteUser.id;

							return (
								<p key={index}>
									<a href={userPath}>
										{favouriteUser.firstName}{' '}
										{favouriteUser.lastName}
									</a>
									<FavouriteUser
										key={index}
										status={this.getFavouriteUserStatus(
											favouriteUser.id,
										)}
										addToFavouriteUsers={() =>
											this.addToFavouriteUsers(
												favouriteUser.id,
											)
										}
										removeFromFavouriteUsers={() =>
											this.removeFromFavouriteUsers(
												favouriteUser.id,
											)
										}
									/>
								</p>
							);
						})}
					</div>
				</ul>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		userId: state.auth.id,
	};
};

export default connect(
	mapStateToProps,
	null,
)(FavouriteUsers);
