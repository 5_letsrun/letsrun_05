import React from 'react';
import connect from "react-redux/es/connect/connect";
import classnames from 'classnames';

import FontIcon from '../../FontIcon/FontIcon';
import Axios from "../../../axios";

import './FavouriteButton.css';

class FavouriteButton extends React.Component {
	state = {
		routeName: '',
		isRouteFavourite: false,
		showInput: false,
	};

	constructor(props) {
		super(props);
		this.inputRef = React.createRef();
	}

	componentDidMount() {
		const {userId, routeId} = this.props;
		Axios.get(`/routes/favorite/${userId}`)
			.then(response => {
				this.setState({
					isRouteFavourite: response.data.favouriteRoutes.filter(route => route.route_id === routeId).length !== 0
				})
			});
	}

	onAddHandler = () => {
		const {userId, routeId} = this.props;
		const {routeName} = this.state;
		if (routeName) {
			Axios.post(`/routes/favorite/${userId}`, {
				routeId,
				routeName
			}).then(() => {
				this.setState({
					isRouteFavourite: true,
					showInput: false,
				})
			});
		}
	};

	onTypingHandler = (e) => {
		this.setState({
			routeName: e.target.value,
		});
	};

	onClickHandler = () => {
		const {isRouteFavourite} = this.state;
		if (!isRouteFavourite) {
			this.setState({
				showInput: true,
			});
		} else {
			const {userId, routeId} = this.props;
			Axios.post(`/routes/delete-favourite`, {
				userId,
				routeId,
			}).then(() => {
				this.setState({
					isRouteFavourite: false,
				})
			});
		}
	};

	render() {
		const {routeName, isRouteFavourite, showInput} = this.state;
		return (
			<div className="favourite-button row align-items-center">
				<div className="col-2">
					<button
						onClick={this.onClickHandler}
						type="button"
						className={classnames('btn', 'btn-light', isRouteFavourite && 'favourite')}
						title={isRouteFavourite ? 'Remove this route from favourites' : 'Add this route to favourites'}
					>
						{isRouteFavourite
							? <FontIcon icon="heart"/>
							: <FontIcon variant="r" icon="heart"/>
						}
					</button>
				</div>
				{showInput
					&& (
						<div className="col input-group">
							<input
								ref={this.inputRef}
								onKeyPress={e => {
									if (e.key === 'Enter') {
										this.onAddHandler();
									}
								}}
								onChange={this.onTypingHandler}
								value={routeName}
								type="text"
								className="form-control"
								placeholder="Type a name"
							/>
							<div className="input-group-append">
								<button
									onClick={this.onAddHandler}
									className="btn btn-info"
									type="button"
								>
									Add
								</button>
							</div>
						</div>
					)
				}
			</div>
		);
	}
}


const mapStateToProps = state => {
	const {id: userId} = state.auth;
	return {
		userId,
	};
};


export default connect(
	mapStateToProps,
)(FavouriteButton);
