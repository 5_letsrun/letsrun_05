import React from 'react';
import EventHalfDetail from '../EventHalfDetail/EventHalfDetail.js';

import './RunItem.css';

export const RunItem = ({ run }) => {
	const { id, date, time } = run;
	const distance = run.Event.Route.distance;
	const name = run.Event.name;
	var runItemHref = '#runItem' + id;
	var runItemId = 'runItem' + id;
	var eventId = run.Event.id;
	var eventPath = '/event/' + eventId;
	var routeId = run.Event.Route.id;

	return (
		<div id="accordion">
			<a
				className="container rounded runitem btn"
				data-toggle="collapse"
				type="button"
				aria-expanded="true"
				aria-controls="collapseExample"
				id={run.id}
				href={runItemHref}
			>
				<div className="row">
					<div className="col">
						<label>{name}</label>
					</div>
					<div className="col">
						<label>{date}</label>
					</div>
					<div className="col">
						<label>{distance} km</label>
					</div>
					<div className="col">
						<label>{time}</label>
					</div>
				</div>
			</a>

			<div id={runItemId} className="collapse" data-parent="#accordion">
				<a className="btn btn-info" href={eventPath}>
					Open event detail
				</a>
				{<EventHalfDetail id={run.id} routeId={routeId} />}
			</div>
		</div>
	);
};
