	'use strict';
module.exports = (sequelize, DataTypes) => {
	const FavouriteUser = sequelize.define(
		'FavouriteUser',
		{},
		{
			underscored: true,
			tableName: 'favourite_users',
		},
	);
	FavouriteUser.associate = function(models) {
		FavouriteUser.belongsTo(models.User, {
			as: 'User',
			foreignKey: 'user_id',
		});

		FavouriteUser.belongsTo(models.User, {
			as: 'Friend',
			foreignKey: 'friend_id',
		});
	};
	return FavouriteUser;
};
