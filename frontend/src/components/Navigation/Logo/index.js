import React from 'react';
import FontIcon from '../../FontIcon/FontIcon';
import Aux from '../../hoc/Auxi/Auxi';

import './index.css';

export default props => (
	<Aux>
		<FontIcon icon="shoe-prints" /> <span id="logo">Let's Run</span>
	</Aux>
);
