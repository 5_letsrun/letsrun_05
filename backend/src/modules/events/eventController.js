import db from '../../models';
import {getRunByUserIdAndEventId} from "../runs/runController";

const Event = db.Event;
const User = db.User;
const Route = db.Route;
const Comment = db.Comment;
const Participant = db.Participant;

const UNAVAILABLE_TO_CREATE_COMMENT = 'Comment couldn\'t be saved. Please try it again later.';

export const getAllEvents = async (req, res) => {

	const where = [];
	const {userId, term, showMyEvents} = req.query;

	if (showMyEvents === '1') {
		where.push({user_id: userId});
	}

	if (term) {
		where.push({
				term: db.sequelize.where(
					db.sequelize.fn('DATE', db.sequelize.col('term')),
					'=',
					term
				)
			}
		)
	} else {
		const date = new Date();
		date.setHours(date.getHours() - 6);
		where.push({
			term: {
				$gt: date,
			},
		})
	}

	const result = await Event.findAll({
		include: [
			Route
		],
		where: {
			$and: where,
		}
	});

	const events = [];
	if (result) {
		await Promise.all(result.map(async (event) => {
			const run = await getRunByUserIdAndEventId(userId, event.id);
			if (run === null) {
				events.push(event)
			}
		}));
	}

	res.status(200).json({events});
};

export const getEventDetail = async (req, res) => {

	const event = await Event.findById(req.params.id, {
		include: [
			User,
			Route,
			{
				model: Comment,

				include: {
					model: User,
				},
			},
			'Participants',
		],
		order:[
			[
				Comment, 'created_at', 'DESC',
			]
		],
	});

	res.status(200).json({ data: event });
};

export const addComment = async (req, res) => {
	const { userId, eventId, content } = req.body;

	Comment.create({
		content,
		eventId,
		userId,
	})
		.then(comment => {
			const {id, content, created_at} = comment;
			res.status(201).json({
				comment: {
					id,
					content,
					created_at,
				},
			});
		})
		.catch(() => {
			res.status(400).json({error: UNAVAILABLE_TO_CREATE_COMMENT})
		});
};

export const participate = async (req, res) => {
	Participant.upsert({
		user_id: req.body.userId,
		eventId: req.body.id,
	});

	res.status(201).end();
};

export const newEvent = async (req, res) => {
	const {
		name,
		description,
		term,
		user_id,
		trace,
		distance,
		start,
		routeId,
	} = req.body;

	let route_id;
	let startPoint = { type: 'Point', coordinates: [start.lng, start.lat] };
	let distanceKm = distance / 1000; //convert from m to km
	if(!routeId) {
		await Route.create({
			start: startPoint,
			trace,
			distance: distanceKm,
			userId: user_id,
		}).then(function (route) {
			route_id = route.id;
		});
	} else {
		route_id = routeId;
	}

	const event = await Event.create({
		name,
		description,
		term,
		route_id,
		user_id,
	});

	Participant.upsert({
		eventId: event.id,
		user_id,
	});

	res.status(201).json({
		event,
	});
};

export const isEventCompleteByUser = async (req, res) => {
	const {userId, eventId} = req.query;

	const run = await getRunByUserIdAndEventId(userId, eventId);

	res.status(200).json({ isEventComplete: run !== null });
};
