'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.addColumn(
			'favourite_routes',
			'name',
			{
				allowNull: true,
				type: Sequelize.STRING(60),
			}
		);
	},

	down: (queryInterface) => {
		return queryInterface.removeColumn(
			'favourite_routes',
			'name',
		);
	}
};
