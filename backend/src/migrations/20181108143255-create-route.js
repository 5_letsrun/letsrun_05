'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface
			.createTable('routes', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				start: {
					allowNull: false,
					type: Sequelize.GEOMETRY('POINT'),
				},
				trace: {
					allowNull: false,
					type: Sequelize.JSON,
				},
				distance: {
					allowNull: false,
					type: Sequelize.DECIMAL(8, 2).UNSIGNED,
				},
				user_id: {
					allowNull: false,
					type: Sequelize.INTEGER.UNSIGNED,
				},
				created_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updated_at: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			})
			.then(() => {
				queryInterface.addConstraint('routes', ['user_id'], {
					type: 'foreign key',
					references: {
						table: 'users',
						field: 'id',
					},
				});

				queryInterface.addIndex('routes', {
					fields: ['start'],
					type: 'SPATIAL',
				});
			});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('routes');
	},
};
