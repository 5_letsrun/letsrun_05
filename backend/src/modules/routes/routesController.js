import db from '../../models';

const Route = db.Route;
const FavouriteRoute = db.FavouriteRoute;

export const getFavouriteRoutes = async (req, res) => {
	const favouriteRoutes = await FavouriteRoute.findAll({
		where: {
			user_id: req.param('id'),
		}
	});

    res.status(200).json({ favouriteRoutes });
};

export const addFavouriteRoutes = async (req, res) => {
	const {routeId, routeName} = req.body;
	FavouriteRoute.create({
		user_id: req.param('id'),
		route_id: routeId,
		name: routeName,
	})
		.then(() => {
			res.status(201).end();
		});
};

export const deleteFavouriteRoute = async (req, res) => {
	const {userId, routeId} = req.body;
	FavouriteRoute.destroy({
		where: {
			user_id: userId,
			route_id: routeId,
		}
	})
		.then(() => {
			res.status(201).end();
		});
};


export const getAllRoutes = async (req, res) => {
	var routes = await Route.findAll();
	routes.foreach(route => {
		route.start = {
			lng: route.start.coordinates[0],
			lat: route.start.coordinates[1],
		};
	});
	res.status(200).json({ data: routes });
};

export const getRoute = async (req, res) => {
	var route = await Route.findById(req.params.id);
	route.start = {
		lng: route.start.coordinates[0],
		lat: route.start.coordinates[1],
	};
	res.status(200).json({ data: route });
};

export const setRoute = async (req, res) => {
	var { id, start, trace, distance, userId } = req.body;
	start = { type: 'Point', coordinates: [start.lng, start.lat] };
	var route;
	if (id) {
		route = await Route.update(
			{ start, trace, distance, userId },
			{ where: { id } },
		);
		start = { lng: start.coordinates[0], lat: start.coordinates[1] };
		res.status(200).json({ data: { id, start, trace, distance, userId } });
	} else {
		route = await Route.create({ start, trace, distance, userId });
		route.start = {
			lng: route.start.coordinates[0],
			lat: route.start.coordinates[1],
		};
		res.status(200).json({ data: route });
	}
};
