'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		const commentsPromise = queryInterface.bulkInsert(
			'comments',
			[
				{
					content: 'Start se posouvá o hodinu.',
					event_id: 1,
					user_id: 1,
					created_at: new Date(),
					updated_at: new Date(),
				},
			],
			{},
		);

		return Promise.all([commentsPromise]);
	},

	down: (queryInterface, Sequelize) => {
		return Promise.all([queryInterface.bulkDelete('comments', null, {})]);
	},
};
