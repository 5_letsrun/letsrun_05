import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import EventsMap from '../../components/EventsMap/EventsMap';
import FontIcon from '../../components/FontIcon/FontIcon';
import Axios from '../../axios';
import PageTitle from '../../components/PageTitle/PageTitle';

import './Homepage.css';

class HomePage extends Component {
	state = {
		events: null,
	};

	eventFilters = {};

	componentDidMount() {
		this.fetchEvents();
	}

	onFilterChangeHandler = event => {
		const target = event.target;
		let targetName = target.name;
		let value = target.value;

		if (targetName === 'showMyEvents') {
			value = target.checked ? 1 : 0;
		}

		this.eventFilters = {
			...this.eventFilters,
			[targetName]: value,
		};
		this.fetchEvents(this.eventFilters);
	};

	fetchEvents = (additionalParams = {}) => {
		const params = {
			userId: this.props.userId,
			...additionalParams,
		};
		Axios.get(`/events/`, {
			params
		}).then(response => {
			this.setState({ events: response.data.events });
		});
	};

	render() {
		const { events } = this.state;

		return (
			<div className="container" id="homepage">
				<PageTitle>Dashboard</PageTitle>
				<Link to="/newEvent">
					<button id="create-button" className="btn btn-info">
						Create new event
					</button>
				</Link>
				<div className="my-jumbotron">
					<form className="form-inline">
						<div className="input-group mb-2 mr-sm-2">
							<div className="input-group-prepend">
								<div className="input-group-text">
									<FontIcon icon="calendar-alt" />
								</div>
							</div>
							<input
								onChange={this.onFilterChangeHandler}
								name="term"
								type="date"
								className="form-control"
							/>
						</div>

						<div className="form-check mb-2 mr-sm-2">
							<input
								onChange={this.onFilterChangeHandler}
								name="showMyEvents"
								className="form-check-input"
								type="checkbox"
								id="inlineFormCheck"
							/>
							<label
								className="form-check-label"
								htmlFor="inlineFormCheck"
							>
								Show my events
							</label>
						</div>
					</form>
				</div>
				<EventsMap events={events} />
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { flashMessages } = state.share;

	return {
		userId: state.auth.id,
		flashMessages,
	};
};


export default connect(
	mapStateToProps,
	null,
)(HomePage);
