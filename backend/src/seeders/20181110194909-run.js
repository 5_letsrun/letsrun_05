'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		const runsPromise = queryInterface.bulkInsert(
			'runs',
			[
				{
					time: new Date(0, 0, 0, 1, 30, 15, 0),
					event_id: 1,
					user_id: 1,
					created_at: new Date(),
					updated_at: new Date(),
				},
				{
					time: new Date(0, 0, 0, 1, 20, 0, 0),
					event_id: 1,
					user_id: 2,
					created_at: new Date(),
					updated_at: new Date(),
				},
			],
			{},
		);

		return Promise.all([runsPromise]);
	},

	down: (queryInterface, Sequelize) => {
		return Promise.all([queryInterface.bulkDelete('runs', null, {})]);
	},
};
