'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		const favouriteUsersPromise = queryInterface.bulkInsert(
			'favourite_users',
			[
				{
					user_id: 1,
					friend_id: 2,
					created_at: new Date(),
					updated_at: new Date(),
				},
				{
					user_id: 2,
					friend_id: 1,
					created_at: new Date(),
					updated_at: new Date(),
				},
			],
			{},
		);

		return Promise.all([favouriteUsersPromise]);
	},

	down: (queryInterface, Sequelize) => {
		return Promise.all([
			queryInterface.bulkDelete('favourite_users', null, {}),
		]);
	},
};
